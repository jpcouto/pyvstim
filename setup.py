#!/usr/bin/env python
# Install script for visual stimulation tools
# Joao Couto - July 2017

import os
from os.path import join as pjoin
from setuptools import setup
from setuptools.command.install import install

longdescription = '''Visual stimulation presentation and logging'''
data_path = pjoin(os.path.expanduser('~'), 'pyvstim-run-prot')

setup(
  name = 'pyvstim',
  version = '0.0',
  author = 'Joao Couto',
  author_email = 'jpcouto@gmail.com',
  description = (longdescription),
  long_description = longdescription,
  license = 'GPL',
  packages = ['pyvstim'],
  entry_points = {
        'console_scripts': [
          'pyvstim-run-prot = pyvstim.presentation:main',
          'pyvstim = pyvstim.gui:main',
        ]
        },
#    data_files=[(data_path, ['path']),
#    ],

    )
