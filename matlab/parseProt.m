function prot_data=parseProt(varargin)
if nargin>=1&&~isempty(varargin{1})
    prot_file_name=varargin{1};
else
    prot_file_name=[];
end

if isempty(prot_file_name) || ~exist(prot_file_name,'file')
    prot_data=[];
    return
else
    
    fid=fopen(prot_file_name,'r');
    logdata=textscan(fid,'%s','delimiter','\');
    logdata = logdata{1};
    fclose(fid);
    
    N=length(logdata);
    parameters=struct();
    par_matrix=struct();
    iPar=0;
    %%
    for idx=1:N
        str=logdata{idx};
        if isempty(strfind(str,'#'))
            
            if ~isempty(strfind(str,'='))
                %%% Get the parameters:
                parts=strsplit(str,{' ','='});
                key=parts{1};
                value=parts{2};
                if ~isnan(str2double(value))
                    value=str2double(value);
                end
                parameters.(key)=value;
            elseif  ~isempty(strfind(str,'dur')) || ~isempty(strfind(str,'pad'))% column headers
                columns=strsplit(str,{' ','\t'});
                %nCols=length(columns);
            else % data
                %%% get parameters
                parts=strsplit(str,{' ','\t'});
                iPar=iPar+1;
                for iCol=1:length(parts)%nCols
                    col_name=columns{iCol};
                    value=str2double(parts{iCol});
                    par_matrix(iPar).(col_name)=value;
                end
            end
        end
    end
    prot_data.parameters=parameters;
    prot_data.par_matrix=par_matrix;
    % extract data using e.g. cat(1,prot_data.par_matrix.n)
    prot_data.nPars=iPar; % nConditions
end