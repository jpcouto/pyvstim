// Simple servo control for placing a pole in front of the mouse
#include <Servo.h>
volatile int angle = 90;
volatile int current_angle = 90;
volatile bool state = false;
const byte servoPin = 8;
const byte ttlPin = 3;
const byte statePin = 7;

volatile long duration = 3000; // time to stand still

Servo servo;
volatile byte command, val;
volatile int i;

#define STX '@'
#define ETX '\n'
#define SEP "_"

#define NOW 'N'
#define ARM 'A'
#define FLIP 'F'
#define SET_PARAMETERS 'P'

// Serial communication "receive"
# define MSGSIZE 64
char msg[MSGSIZE];
int cnt = 0;

volatile bool is_armed = false;
volatile bool is_ready = false;
unsigned long tstart;
volatile unsigned long curtime = 0;

void ttl_trigger() {
  if (is_armed == true) {
    state = true;
  }
}

void setup() {
  servo.attach(servoPin);
  pinMode(statePin, OUTPUT);
  pinMode(ttlPin, INPUT);
  servo.write(angle);
  digitalWrite(statePin, LOW);
  //digitalWrite(ttlPin, LOW);
  Serial.begin(115200);
  while (!Serial);
  Serial.println("Started");
  attachInterrupt(digitalPinToInterrupt(ttlPin), ttl_trigger, FALLING);
//  attachInterrupt(digitalPinToInterrupt(statePin), trig, RISING);

  tstart = millis();
}

void loop() {
  curtime = millis() - tstart;
  bool last_state = false;
  if (last_state != state) {
    trig();
    last_state = state;
  }
}

void trig() {
  if (state == true) {
    servo.write(angle-90);
    delay(duration);
    servo.write(angle);
    state = false;
    digitalWrite(statePin, LOW);
    is_ready = false;      
    is_armed = false;
  }
}

void serialEvent()
{
  while (Serial.available()) {
    char ch = Serial.read();
    if (ch == STX || cnt > 0) {
      msg[cnt] = ch;
      cnt++;
      if (ch == ETX) {
        // Message completed, process it.
        cnt = 0;
        String reply = String(STX);
        switch (msg[1]) {
          case ARM:
            // Arm to wait for a TTL trigger
            // @A_time
            is_ready = false;
            is_armed = true;
            reply += ARM;
            Serial.print(reply);
            Serial.print(SEP);
            Serial.print(curtime);
            Serial.print(ETX);
            break;
         case NOW:
            // Arm to wait for a TTL trigger
            // @N_time
            is_ready = true;
            is_armed = false;
            state = true;
            digitalWrite(statePin, HIGH);
            reply += NOW;
            Serial.print(reply);
            Serial.print(SEP);
            Serial.print(curtime);
            Serial.print(ETX);
         case FLIP:
            if (current_angle == angle) {
              servo.write(angle - 90);
              current_angle = angle-90;
            }
            else {
              servo.write(angle);
              current_angle = angle;
            }
            reply += FLIP;
            Serial.print(reply);
            Serial.print(SEP);
            Serial.print(curtime);
            Serial.print(ETX);
            break;
    case SET_PARAMETERS:
      setParameters(msg);
      reply += SET_PARAMETERS;
      Serial.print(reply);
      Serial.print(SEP);
      Serial.print(duration);
      Serial.print(ETX);
      break;
          default:
            reply += "E";
            reply += 1;
            reply += ETX;
            Serial.print(reply);
            break;
        }
      }
    }
  }
  Serial.flush();
}

void setParameters(char* msg)
{
  // parameters are formated like: NPULSES_WIDTH_MARGIN
  char* token;
  // Parse string using a (destructive method) 
  token = strtok(msg, SEP);
  token = strtok(NULL, SEP);
  duration  = atoi(token);
  // need to acknowledge that this was set
}

