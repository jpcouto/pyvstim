
// Simple e optogenetics pulse generator
// Adapted from Dario Ringarch DLR 10/30/17

#define CPMS 3108    // how many counts in 1ms (have to measure for each board)

const byte ledPin = 13;
const byte interruptPin = 2;
const byte trigPin = 8;
const byte gatePin = 9;
const byte ttlPin = 3;

volatile byte npulses = 10; // number of pulses to deliver
volatile byte width = 10; // width of pulses in msec
volatile byte margin = 2; // offset margin in ms
volatile byte pulseCount = 0; // pulses left to deliver

volatile byte command, val;
volatile int i;

#define STX '@'
#define ETX '\n'
#define SEP "_"

#define NOW 'N'
#define ARM 'A'
#define SET_PARAMETERS 'P'

// Serial communication "receive"
# define MSGSIZE 64
char msg[MSGSIZE];
int cnt = 0;

volatile bool is_armed = false;
volatile bool is_ready = false;
unsigned long tstart;
volatile unsigned long curtime = 0;

void ttl_trigger() {
  if (is_armed == true) {
    Serial.println("@T");
    pulseCount = npulses;
    is_ready = true;

  }
}

void setup() {

  pinMode(ledPin, OUTPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(gatePin, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  pinMode(ttlPin, INPUT);
  digitalWrite(ledPin, LOW);
  digitalWrite(gatePin, LOW);
  digitalWrite(trigPin, LOW);
  digitalWrite(ttlPin, LOW);
  Serial.begin(115200);
  attachInterrupt(digitalPinToInterrupt(interruptPin), trig, FALLING);
  attachInterrupt(digitalPinToInterrupt(ttlPin), ttl_trigger, FALLING);
  tstart = millis();
}

void loop() {
  curtime = millis() - tstart;
}

// Note that some Arduino boards cannot use delayMicroseconds() in an interrupt routine
// If this is this case for yorus, you have to switch the digitalMicroseconds line with
// the for loop, which will provide the delay.  You will have to calibrate this in terms
// the number of counts per msec required.  This is then the value you can use to define
// the CPMS constant above.

void trig() {
  if (is_ready == true) {
    Serial.println(pulseCount);
    if (pulseCount > 0) { // note that delayMicroseconds() does not work in ISR
      digitalWrite(gatePin, HIGH);
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(1000*width);
      //for (i = 0; i < width * CPMS; i++); // delay
      digitalWrite(trigPin, LOW);
      delayMicroseconds(1000*margin);
      //for (i = 0; i < margin * CPMS; i++); // delay
      digitalWrite(gatePin, LOW);
      pulseCount--;
    } else {
      is_ready = false;
      pulseCount = npulses;
      is_armed = false;
    }
  }
}

void serialEvent()
{
  while (Serial.available()) {
    char ch = Serial.read();
    if (ch == STX || cnt > 0) {
      msg[cnt] = ch;
      cnt++;
      if (ch == ETX) {
        // Message completed, process it.
        cnt = 0;
        String reply = String(STX);
        switch (msg[1]) {
          case ARM:
            // Arm to wait for a TTL trigger
            // @A_time
	          is_ready = false;
	          is_armed = true;
	          pulseCount = npulses;
            reply += ARM;
            Serial.print(reply);
            Serial.print(SEP);
            Serial.print(curtime);
            Serial.print(ETX);
            break;
         case NOW:
            // Arm to wait for a TTL trigger
            // @N_time
            is_ready = true;
            is_armed = false;
            pulseCount = npulses;
            reply += NOW;
            Serial.print(reply);
            Serial.print(SEP);
            Serial.print(curtime);
            Serial.print(ETX);
            break;
	  case SET_PARAMETERS:
	    setParameters(msg);
      pulseCount = npulses;
      reply += SET_PARAMETERS;
      Serial.print(reply);
      Serial.print(SEP);
      Serial.print(npulses);
      Serial.print(SEP);
      Serial.print(width);
      Serial.print(SEP);
      Serial.print(margin);
      Serial.print(ETX);
	    break;
          default:
            reply += "E";
            reply += 1;
            reply += ETX;
            Serial.print(reply);
            break;
        }
      }
    }
  }
  Serial.flush();
}

void setParameters(char* msg)
{
  // parameters are formated like: NPULSES_WIDTH_MARGIN
  char* token;
  // Parse string using a (destructive method) 
  token = strtok(msg, SEP);
  token = strtok(NULL, SEP);
  npulses  = atoi(token);
  token = strtok(NULL, SEP); 
  width = atoi(token);
  token = strtok(NULL, SEP); 
  margin = atoi(token);
  // need to acknowledge that this was set
}

