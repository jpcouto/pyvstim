from PyQt5.QtWidgets import (QWidget,
                             QApplication,
                             QGridLayout,
                             QFormLayout,
                             QVBoxLayout,
                             QHBoxLayout,
                             QTabWidget,
                             QCheckBox,
                             QPushButton,
                             QInputDialog,
                             QTextEdit,
                             QLineEdit,
                             QSlider,
                             QComboBox,
                             QLabel,
                             QAction,
                             QProgressBar,
                             QMenuBar,
                             QGraphicsView,
                             QGraphicsScene,
                             QGraphicsItem,
                             QGraphicsLineItem,
                             QGroupBox,
                             QTableWidget,
                             QMainWindow,
                             QDockWidget,
                             QFileDialog,
                             QDesktopWidget,
                             QTreeView)
from PyQt5.QtGui import (QImage, QPixmap,QBrush,QPen,
                         QColor,
                         QStandardItemModel,
                         QStandardItem)
from PyQt5.QtCore import Qt,QSize,QRectF,QLineF,QPointF,QTimer
from PyQt5.QtTest import QTest
qWait = QTest.qWait

from .presentation import *
from .loggers import Logger
from multiprocessing import Queue
from datetime import datetime,date
import sys
import types
import os
from os.path import join as pjoin
import time
from glob import glob
from shutil import copyfile
from .utils import display
from .controllers import *

class VStimGUI(QMainWindow):
    app = None
    rig = None
    logger = None
    experiment = None
    stims = None
    def __init__(self, app = None,
                 user = None,
                 expname = 'test',
                 preferences= {}):
        super(VStimGUI,self).__init__()
        display('Opening GUI')
        if 'GUIscreen' in preferences.keys(): 
            monitor = QDesktopWidget().screenGeometry(preferences['GUIscreen'])
            self.move(monitor.left(), monitor.top()+20)
        self.expname = expname
        self.app = app
        bar = self.menuBar()
        editmenu = bar.addMenu('Experiment')
        editmenu.addAction('Run multiple protocols')
        editmenu.addAction('Reload user protocols')
        editmenu.addAction('Change user')
        editmenu.addAction('Set run number')
        editmenu.addAction('User controlled gratings')
        editmenu.triggered[QAction].connect(self.experimentMenuActions)


        self.setWindowTitle('Visual stimulation control window')
        # Set experiment stuff
        self.preferences = preferences

        self.tabs = []
        # rig control tab
        self.tabs.append(self.initLoggerUI())
        self.tabs.append(self.initRigUI())
        self.tabs.append(self.initExperimentUI())

        pmtui = self.initPMTGateUI()
        if not pmtui is None:
            self.tabs.append(pmtui)
        if not self.PMTgate is None:
            self.exp.PMTgate = self.PMTgate

        servoui = self.initServoStageUI()
        if not servoui is None:
            self.tabs.append(servoui)
        if not self.ServoStage is None:
            self.exp.ServoStage = self.ServoStage

        layout = QVBoxLayout()
        self.addDockWidget(Qt.BottomDockWidgetArea,self.tabs[0])
        self.addDockWidget(Qt.TopDockWidgetArea,self.tabs[2])
        self.addDockWidget(Qt.BottomDockWidgetArea,self.tabs[1])

        for tab in self.tabs[3:]:
            self.addDockWidget(Qt.TopDockWidgetArea,tab)

        # experiment control tab
        self.show()

    def initLoggerUI(self):
        self.logQ = Queue()
        self.logger = Logger(inQ = self.logQ,
                             preferences = self.preferences)
        #self.logger.start()
        #self.logger.startlog()
        widget = QDockWidget('Logger',self)
        self.loggerController = LoggerController(self.logger,self.preferences)
        widget.setWidget(self.loggerController)
        display('Initialized the logger controller.')
        return widget
        
    def initRigUI(self,port = None):
        widget = QDockWidget('Rig',self)
        if 'rig' in self.preferences.keys():
            port = self.preferences['rig']['port']
        if port is None:
            self.rig = None
        else:
            from rig import BehaviorRig
            self.rig = BehaviorRig(port = port,
                                   logger = self.logger,
                                   resetLog = False)
            from rig.widgets import RigWidget

            self.rigController = RigWidget(self.rig)
            widget.setWidget(self.rigController)
        return widget

    def initPMTGateUI(self,port = None):
        widget = None
        if 'PMTgate' in self.preferences.keys():
            port = self.preferences['PMTgate']['port']
        if port is None:
            self.PMTgate = None
        else:
            widget = QDockWidget('PMT gating',self)
            from .gatingduino import PMTGateInterface
            self.PMTgate = PMTGateInterface(port = port)
            self.PMTgateController = PMTGatingController(self.PMTgate)
            widget.setWidget(self.PMTgateController)
        return widget

    def initServoStageUI(self,port = None):
        widget = None
        if 'ServoStage' in self.preferences.keys():
            port = self.preferences['ServoStage']['port']
            stage = self.preferences['ServoStage']['stage']
        if port is None:
            self.ServoStage = None
        else:
            widget = QDockWidget('PMT gating',self)
            from .servostim_control import ServoStageControl
            self.ServoStage = ServoStageControl(duinoport = port,stage_number=stage)
            self.ServoStageController = ServoStageController(self.ServoStage)
            widget.setWidget(self.ServoStageController)
        return widget

    
    
    def initExperimentUI(self):
        prefs = self.preferences
        from psychopy import monitors
        if not 'units' in prefs.keys():
            prefs['units'] = 'deg'
        self.units = prefs['units']
        for screen in prefs['monitor']:
            mon = monitors.Monitor(screen['name'])
            mon.setWidth(screen['width'])
            mon.setSizePix(screen['sizePix'])
            mon.setGamma(screen['gamma'])
            mon.setDistance(screen['distance'])
            mon.saveMon()
        self.monitor = prefs['monitor']
        if not 'flashIndicatorParameters' in prefs.keys():
            prefs['flashIndicatorParameters'] = None
        if not 'warp' in prefs.keys():
            prefs['warp'] = None
            
        from .psych.experiment import Experiment
        self.exp = Experiment(monitor = self.monitor,
                              logger = self.logger,
                              pulseDevice=None,
                              units=self.units,
                              rig = self.rig,
                              flashIndicator=prefs['flashIndicator'],
                              flashIndicatorParameters = prefs['flashIndicatorParameters'],
                              warp = prefs['warp'],
                              progress = None,
                              gui = self)
        widget = QDockWidget('Experiment',self) 
        self.expController = ExperimentController(experiment=self.exp,
                                                  expname = self.expname,
                                                  preferences = self.preferences,
                                                  loggerController = self.loggerController,
                                                  parent=self)
        widget.setWidget(self.expController)
        return widget
    
    def experimentMenuActions(self,q):
        display(q.text()+ "clicked. ")
        if q.text() == 'Change user':
            text, ok = QInputDialog.getText(self, 'Change user','Enter user name:')
            if ok:
              self.preferences['user'] = text
              from .utils import getPreferences
              prefs = getPreferences(user = text, no_create=True)
              if not prefs is None:
                  self.preferences = prefs
              else:
                  displat('User does not exist [{0}]'.format(text))
              self.expController.preferences = self.preferences
              self.loggerController.preferences = self.preferences
              self.expController.protocolSelector._updateTree()
              display('Warning: This does not fully load a new user (good enough to reload preferences though...)!')
        elif q.text() == 'Set run number':
            text, ok = QInputDialog.getText(self, 'Specify run number','Set run number:')
            if ok:
                try:
                    num = int(text)
                    self.exp.setSessionNumber(num)
                    display('Experiment run number is: {0}. Please re-load stim to apply.'.format(num))
                except:
                    display('That does not seem like a valid number. Did nothing.')
        elif q.text() == 'Reload user protocols':
            self.expController.protocolSelector._updateTree()
            display('Updated protocol filenames.')
        elif q.text() == 'Run multiple protocols':
            display('Starting the batch controller.')
            widget = QDockWidget('Batch controller',self) 
            batch_control = BatchExperimentController(self.expController)
            widget.setWidget(batch_control)
            self.addDockWidget(Qt.RightDockWidgetArea and
                               Qt.BottomDockWidgetArea,widget)
        elif q.text() == 'User controlled gratings':
            from .psych.stimulus import UserControlledGrating
            stim = UserControlledGrating(self.exp)
            self.exp.run([stim],waitForRigPulses = None)
            self.exp.setKeyActions()
    def update(self):
        self.app.processEvents()

    def closeEvent(self,event):
        display('Closing connection to the rig and cleaning up screen.')
        self.exp.close()
        self.logger.stoplog()
        self.logger.close()
        display("Closed logger and experiment.")
        if hasattr(self,"rig"):
            if not self.rig is None:
                self.rig.close()
                display("Closed rig.")
        if hasattr(self,"PMTGate"):
            if not self.PMTGate is None:
                self.PMTGate.close()
        event.accept()

class LoggerController(QWidget):
    def __init__(self,logger,preferences):
        super(LoggerController,self).__init__()
        self.preferences = preferences
        self.logger = logger
        self.iRun = 0
        form = QFormLayout()
        # Need to set the log name and folder. 
        self.logStatus = QLabel(self.logger.getFilename())
        form.addRow(self.logStatus)
        self.commentEdit = QLineEdit(' ')
        self.commentEdit.returnPressed.connect(self.buttonComment)
        startButton = QPushButton('Start logging')
        startButton.clicked.connect(self.startLogging)
        form.addRow(self.commentEdit)
        form.addRow(startButton)
        self.setLayout(form)
    def incrementRun(self):
        self.iRun += 1
        display('Incremented run number [{0}]'.format(self.iRun))
    def buttonComment(self):
        txt = self.commentEdit.text()
        if not txt.startswith('#'):
            txt = '# ' + txt 
        self.logger.put(txt)
        display(txt + ' sent comment to logger.')
        #display(self.logger.inQ.get())
    def startLogging(self,log_directory = None, protocol_filename = None):
        '''
        Creates protocol file and preference copies  and starts logging.
        '''
        if not log_directory is None:
            self.logger.setFileName(log_directory+'.log')
            self.logStatus.setText(log_directory+'.log')

        if not self.logger.is_alive():
            self.logger.start()
            display('Starting the logger daemon.')

        if not protocol_filename is None:
            fname,ext = os.path.splitext(protocol_filename)
            copypath = pjoin(os.path.abspath(self.preferences['logFolder']),
                             log_directory+ext)
            if not os.path.exists(os.path.dirname(copypath)):
                os.makedirs(os.path.dirname(copypath))
            copyfile(protocol_filename,copypath)
            with open(pjoin(os.path.abspath(self.preferences['logFolder']),
                            log_directory+'.prefs'),'w') as outfile:
                import json
                json.dump(self.preferences,
                          outfile,
                          sort_keys = True,
                          indent = 4)

        if not self.logger.is_alive():
            display('''
            
            Could not start the data log. 

            Nothing will be saved.

            You'll need to restart pyvstim; tell someone if this happens often.
''')
            return
        self.logger.startlog()
        fname = self.logger.getFilename()
        display('Logging to {0}.'.format(fname))
        
    def stopLogging(self):
        display('Stopped logger')
        self.logger.stoplog()

class ProtocolSelector(QTreeView):
    def __init__(self,protocolsFolder = None,**kwargs):
        QTreeView.__init__(self)
        self.protocolsFolder = protocolsFolder

        self.root_model = QStandardItemModel()
        self.setModel(self.root_model)
        
        self.setSelectionMode(QTreeView.ExtendedSelection)

        self._updateTree()
        self.expandToDepth(1)
        self.root_model.setHeaderData(0,Qt.Horizontal, "Protocols:")
        
    def _getTree(self,protocolsFolder):
        assert os.path.isdir(protocolsFolder), "Folder {0} does not exist".format(protocolsFolder)
        tree = {}
        for root, dirs, files in os.walk(protocolsFolder):
            path = root.split(os.sep)
            base = os.path.basename(root)
            if base == os.path.basename(protocolsFolder):
                base = ''
            tree[base] = []
            for f in files:
                tree[base].append(f)

        return tree

    def _updateTree(self):
        self.root_model.clear()
        self._populateTree(self._getTree(self.protocolsFolder),self.root_model)
        
    
    def get(self):
        selection = self.selectedIndexes()
        files = []
        for sel in selection:
            if sel.parent().data() is None:
                files.append(pjoin(self.protocolsFolder,sel.data()))
            else:
                files.append(pjoin(self.protocolsFolder,sel.parent().data(),sel.data()))
        return files
    
    def _populateTree(self,children,parent):
        for child in sorted(children):
            child_item = QStandardItem(child)
            child_item.setEditable(False)
            parent.appendRow(child_item)
            if isinstance(children, type(dict())):
                self._populateTree(children[child], child_item)
        
class ExpProgressBar(QProgressBar):
    def __init__(self):
        super(ExpProgressBar,self).__init__()
        self.total = 3000
        self.setMinimum(0)
        self.setMaximum(self.total)
    def update(self,val = 1):
        self.setValue(self.value() + val)
    def reset(self):
        self.setValue(0)
    def setTotal(self,N):
        self.setMaximum(N)

class BatchExperimentController(QWidget):
    def __init__(self,experimentController,sleepTime = 30):
        '''
        To run protocols in batch
        '''
        self.expController = experimentController
        super(BatchExperimentController, self).__init__()
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.expController.protocolSelector.setSelectionMode(QTreeView.NoSelection)
        self.controllers = self.expController.controllers
        self.sleepTime = sleepTime
        self.filenames = self.expController.protocolSelector.get()
        self.nProtocols = len(self.filenames)
        form = QFormLayout()

        self.runBatchButton = QPushButton('Run batch')
        self.runBatchButton.clicked.connect(self.buttonBatchRun)
        form.addRow(self.runBatchButton)
        self.setLayout(form)
        
    def buttonBatchRun(self):
        print('''

########################################
###########Batch of experiments#########
########################################
''')
        print('\n\n'.join(self.filenames))
        print('########################################')
        self.runBatchButton.setEnabled(False)
        for iFile in range(self.nProtocols):
            if 'scanbox' in self.expController.controllerNames:
                if self.controllers['scanbox']['toggle'].isChecked():
                    funct = self.expController.controllers['scanbox']['control'].startAcquisition
                else:
                    funct=None
            self.runBatchButton.setStyleSheet('QPushButton {color: red;}')
            self.expController.buttonLoad(index = iFile,skipTrigger=True, onTriggerFunct=funct)
            
            display('Protocol finished. Sleeping for {0} seconds.'.format(self.sleepTime))
            tstart = time.time()
            cnt = 0
            if not iFile == self.nProtocols - 1:
                while (time.time() - tstart) < self.sleepTime:
                    qWait(250)
                    if np.mod(cnt,2) == 0:
                        self.runBatchButton.setStyleSheet('QPushButton {color: red;}')
                    else:
                        self.runBatchButton.setStyleSheet('QPushButton {color: black;}')
                    cnt += 1
        self.runBatchButton.setStyleSheet('QPushButton {color: gray;}')
        self.runBatchButton.setEnabled(True)

        print('''

########################################
        Batch done, please close the batch controller.

''')
            
        # Enable selection
        self.expController.protocolSelector.setSelectionMode(QTreeView.ExtendedSelection)

                
class ExperimentController(QWidget):
    def __init__(self,
                 experiment=None,
                 expname = 'test',
                 preferences=None,
                 loggerController = None,
                 triggerScanBox = False,
                 parent = None):
        super(ExperimentController,self).__init__()
        self.preferences = preferences
        self.parent = parent
        self.exp = experiment
        self.expname = expname
        self.runNumber = 0
        self.loggerController = loggerController

        lay = QVBoxLayout()
        pGroup = QGroupBox()
        form = QFormLayout()
        pGroup.setLayout(form)
        pGroup.setTitle("Experiment")  
        self.pbar = ExpProgressBar()
        self.exp.pbar = self.pbar
        lay.addWidget(self.pbar)
        runButton = QPushButton('Run')
        runButton.setStyleSheet('QPushButton {font-weight: bold;}')
        runButton.clicked.connect(self.buttonRun)
        testStimButton = QPushButton('Test stimuli')
        testStimButton.clicked.connect(self.buttonTestStim)
        form.addRow(testStimButton)
        saveStimButton = QPushButton('Save frames')
        saveStimButton.clicked.connect(self.buttonSaveStim)
        form.addRow(saveStimButton)
        stopButton = QPushButton('Stop')
        stopButton.clicked.connect(self.buttonStop)
        self.expnameLabel = QLabel(self.expname)
        if not 'defaultExperimentType' in preferences.keys():
            self.preferences['defaultExperimentType'] = '2P'
        self.expprefix = QLineEdit(self.preferences['defaultExperimentType'])
        form.addRow(self.expnameLabel,self.expprefix)

        protocolsFolder = self.preferences['protocolsFolder']

        self.protocolSelector = ProtocolSelector(protocolsFolder)
        self.protocolSelector.setMinimumHeight(300)
        self.protocolSelector.setMinimumWidth(500)
        
        form.addRow(self.protocolSelector)        

        loadButton = QPushButton('Load')
        loadButton.setStyleSheet('QPushButton {font-weight: bold;}')

        loadButton.clicked.connect(self.buttonLoad)
        form.addRow(loadButton)
        form.addRow(runButton,stopButton)
        lay.addWidget(pGroup)        
        # Flash Indicator
        pGroup = QGroupBox()
        pform = QFormLayout()
        pGroup.setLayout(pform)
        pGroup.setTitle("Indicator parameters")  
        indpar = self.exp.flashIndicatorParameters
        indsize = QLineEdit(str(indpar['size']))
        def szchange():
            try:
                val = float(indsize.text())
                self.exp.photosensorIndicator.size = val
                self.exp.photosensorIndicator.draw()
                self.exp.flipAndWait()
            except:
                display('Indicator size not set [incorrect value?].')                
        indsize.returnPressed.connect(szchange)
        pform.addRow(QLabel('Size'),indsize)
        indpos = QLineEdit(','.join([ str(k) for k in indpar['pos']]))
        def poschange():
            try:
                val = [float(x) for x in indpos.text().split(',')]
                self.exp.photosensorIndicator.pos = val
                self.exp.photosensorIndicator.draw()
                self.exp.flipAndWait()
            except:
                display('Indicator position not set [incorrect value?].')                
        indpos.returnPressed.connect(poschange)
        pform.addRow(QLabel('Position'),indpos)
        indunits = QLineEdit(indpar['units'])
        def unitschange():
            try:
                self.exp.photosensorIndicator.units = indunits.text()
                self.exp.photosensorIndicator.draw()
                self.exp.flipAndWait()
            except:
                display('Indicator units not set [incorrect value (norm,cm,deg,degFlat,degFlatPos,pix)].')                
        indunits.returnPressed.connect(unitschange)
        pform.addRow(QLabel('Units'),indunits)
        self.indmode = QComboBox()
        self.indmode.addItem('On blank',0)
        self.indmode.addItem('Flicker',1)
        self.indmode.addItem('On stim',2)
        pform.addRow(QLabel('Mode'),self.indmode)
        def modechange(new):
            self.exp.flashIndicatorParameters['mode'] = new
        self.indmode.currentIndexChanged.connect(modechange)
        self.indmode.setCurrentIndex(self.exp.flashIndicatorParameters['mode'])
        lay.addWidget(pGroup)
        # Controllers
        pGroup = QGroupBox()
        pform = QFormLayout()
        pGroup.setLayout(pform)
        pGroup.setTitle("Network  triggers")  
        
        self.controllers = {} # list of available controllers dict(name=,control=,toggle=)
        if 'scanbox' in preferences.keys():
            self.controllers['scanbox'] = dict(control = ScanboxController(
                self.preferences['scanbox']['ip'],
                self.preferences['scanbox']['port']),
                                               toggle = QCheckBox())
            self.controllers['scanbox']['toggle'].setChecked(
                self.preferences['scanbox']['trigger'])
            pform.addRow(QLabel('Load and run scanbox:'),self.controllers['scanbox']['toggle'])
        if 'spikeglx' in preferences.keys():
            if 'root_folder' in self.preferences['spikeglx'].keys():
                spikeglx_root_folder = self.preferences['spikeglx']['root_folder']
            if 'root_folder_map' in self.preferences['spikeglx'].keys():
                spikeglx_root_folder_map = self.preferences['spikeglx']['root_folder_map']
            self.controllers['spikeglx'] = dict(control = SpikeGLxController(
                self.preferences['spikeglx']['ip'],
                self.preferences['spikeglx']['port'],
                root_folder = spikeglx_root_folder,
                root_folder_map = spikeglx_root_folder_map),
                                                toggle = QCheckBox())
            self.controllers['spikeglx']['toggle'].setChecked(
                self.preferences['spikeglx']['trigger'])
            pform.addRow(QLabel('Load and run spikeglx:'),self.controllers['spikeglx']['toggle'])
        if 'labcams' in preferences.keys():
            self.controllers['labcams'] = dict(control = LabcamsController(
                self.preferences['labcams']['ip'],
                self.preferences['labcams']['port']),
                                                      toggle = QCheckBox())
            self.controllers['labcams']['toggle'].setChecked(
                self.preferences['labcams']['trigger'])
            pform.addRow(QLabel('Load and run labcams:'),self.controllers['labcams']['toggle'])

        self.controllerNames = list(self.controllers.keys())
        lay.addWidget(pGroup)
        self.setLayout(lay)

    def buttonRun(self):
        #while not self.loggerController.logger.inQ.empty():
        #    self.loggerController.logger.inQ.get()
        
        display('Experiment: run pressed.')
        exptype = str(self.expprefix.text())
        display('Session number: {0}'.format(self.exp.timesTriggered))
        if not self.exp.waiting:
            display('Press load first.')
            return
        
        if 'spikeglx' in self.controllerNames:
            if self.controllers['spikeglx']['toggle'].isChecked():# and '2P' in exptype:
                self.controllers['spikeglx']['control'].startAcquisition()
                print('\n\n')
                display('Giving spikeglx 5 seconds to start.')
                print('\n\n')

                qWait(5)
        display('Triggering experiment.')
        self.exp.setTrigger()
        
        if 'scanbox' in self.controllerNames:
            if self.controllers['scanbox']['toggle'].isChecked():# and '2P' in exptype:
                self.controllers['scanbox']['control'].startAcquisition()

    def buttonTestStim(self):
        display('Experiment:test stim pressed.')
        protocol = self.protocolSelector.get()
        if len(protocol):
            filename = protocol[0]
            protocol = os.path.basename(protocol[0])
        else:
            display('Could not select protocol.')
            return
        protocol = os.path.splitext(protocol)[0]
        try:
            stims,(stimopt,stimpar) = protfileExperiment(self.exp,
                                                         filename = filename,
                                                         prefs = self.preferences)
        except Exception as e:
            display('Could not load, error follows:')
            print(e)
            display('\n\n\n\ \t\t ERROR: Could not parse protocol!!!' + filename)
            #exc_type, exc_obj, exc_tb = sys.exc_info()
            #fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            #print('Exception {0} on file {1}; line {2}'.format(exc_type, fname, exc_tb.tb_lineno))
            return
        self.exp.logger = None
        if 'IndicatorMode' in stimopt.keys():
            print('Setting indicator mode to: {0}'.format(stimopt['IndicatorMode']))
            indmod = dict(blank=0,flicker = 1,stim = 2)
            if stimopt['IndicatorMode'] in indmod.keys():
                self.exp.flashIndicatorParameters['mode'] = indmod[stimopt['IndicatorMode']]
            else:
                print('Unknown indicator mode.Available: {0}'.format(','.join(indmod.keys())))


        if hasattr(self.parent,'rigController'):
            self.parent.rigController.stop_plots()
            
        self.exp.setTrigger()
        self.exp.run(stims,waitForRigPulses = None)
        self.exp.flashIndicatorParameters['mode'] = self.indmode.currentIndex()

        display('NOT LOGGING!')
        if hasattr(self.parent,'rigController'):
            self.parent.rigController.start_plots()
        qWait(1000)

    def buttonSaveStim(self):
        display('Experiment:save stim pressed.')
        protocol = self.protocolSelector.get()
        if len(protocol):
            filename = protocol[0]
            protocol = os.path.basename(protocol[0])
        else:
            display('Could not select protocol.')
            return
        protocol = os.path.splitext(protocol)[0]
        try:
            stims,(stimopt,stimpar) = protfileExperiment(self.exp,
                                                         filename = filename,
                                                         prefs = self.preferences)
        except Exception as e:
            display('Could not load, error follows:')
            print(e)
            display('\n\n\n\ \t\t ERROR: Could not parse protocol!!!' + filename)
            #exc_type, exc_obj, exc_tb = sys.exc_info()
            #fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            #print('Exception {0} on file {1}; line {2}'.format(exc_type, fname, exc_tb.tb_lineno))
            return
        self.exp.logger = None
        if 'IndicatorMode' in stimopt.keys():
            print('Setting indicator mode to: {0}'.format(stimopt['IndicatorMode']))
            indmod = dict(blank=0,flicker = 1,stim = 2)
            if stimopt['IndicatorMode'] in indmod.keys():
                self.exp.flashIndicatorParameters['mode'] = indmod[stimopt['IndicatorMode']]
            else:
                print('Unknown indicator mode.Available: {0}'.format(','.join(indmod.keys())))


        protocol = os.path.splitext(protocol)[0]
        self.runNumber = self.exp.timesTriggered
        self.expnameLabel.setText('Exp: <b> {0} </b> - <b> run{1:02d}_{2} </b>'.format(self.expname,self.runNumber,protocol))
        todaydatestr = date.today().strftime('%y%m%d')
        datafolder = '{0}_{1}_{2}_{3}'.format(todaydatestr,self.expname,
                                                self.expprefix.text(),
                                                self.preferences['userPrefix'])
        protocolfolder = 'run{0:02d}_{1}'.format(self.runNumber,protocol)
        logdir = pjoin(datafolder,protocolfolder)
        if not os.path.isdir(logdir):
            os.makedirs(logdir)
        self.exp.setTrigger()
        self.exp.run(stims,waitForRigPulses = None,screenshot_folder = logdir)
        self.exp.flashIndicatorParameters['mode'] = self.indmode.currentIndex()

        display('NOT LOGGING!')
        if hasattr(self.parent,'rigController'):
            self.parent.rigController.start_plots()
        qWait(1000)

        
    def buttonLoad(self,index = 0,
                   skipTrigger = False,
                   onTriggerFunct = None,
                   waitTime = 4):
        protocol = self.protocolSelector.get()
        if len(protocol):
            filename = protocol[index]
            protocol = os.path.basename(protocol[index])
        else:
            display('Could not select protocol.')
            return
        protocol = os.path.splitext(protocol)[0]
        try:
            stims,(stimopt,stimpar) = protfileExperiment(self.exp,
                                                         filename = filename,
                                                         prefs = self.preferences)
        except Exception as e:
            print('\n\n\n\ \t\t ERROR: Could not parse protocol!!!' + filename)
            print(e)
            print('Exception {0} on file {1}; line {2}'.format(exc_type, fname, exc_tb.tb_lineno))
            return
        self.protocolFilename = filename
        self.exp.setStims(stims)
        display("Stimuli [{0}] loaded.".format(protocol))
        if 'IndicatorMode' in stimopt.keys():
            print('Setting indicator mode to: {0}'.format(stimopt['IndicatorMode']))
            indmod = dict(blank=0,flicker = 1,stim = 2)
            if stimopt['IndicatorMode'] in indmod.keys():
                self.exp.flashIndicatorParameters['mode'] = indmod[stimopt['IndicatorMode']]
            else:
                print('Unknown indicator mode.Available: {0}'.format(','.join(indmod.keys())))
        self.exp.logger = self.loggerController.logger
        self.setExperimentFoldersAndLog(index = index, waitTime = waitTime)
        exptype = str(self.expprefix.text())
        if '2P' in exptype:
            exptype = 'imaging'
        elif '1P' in exptype:
            exptype = 'cam3'
        else:
            exptype = None
        # then need to start spikeGLx if skiptrigger (for bash runs)
        if ('spikeglx' in self.controllerNames) and skipTrigger:
            if self.controllers['spikeglx']['toggle'].isChecked():
                self.controllers['spikeglx']['control'].startAcquisition()
                display('Giving spikeglx 5 seconds to start.')
                qWait(5)
        if 'Reward' in stimopt.keys():
            rewardMethod = stimopt['Reward']
        else:
            rewardMethod = None
        if hasattr(self.parent,'rigController'):
            self.parent.rigController.stop_plots()
        self.exp.run(waitForRigPulses = exptype,
                     rewardMethod = rewardMethod,
                     skipTrigger = skipTrigger,
                     onTriggerFunct = onTriggerFunct)
        self.exp.flashIndicatorParameters['mode'] = self.indmode.currentIndex()
        if hasattr(self.parent,'rigController'):
            self.parent.rigController.start_plots()

        #if not self.loggerController is None:
        #    self.loggerController.stopLogging()

    def setExperimentFoldersAndLog(self,index = 0, waitTime = 4):
        protocol = self.protocolSelector.get()
        if len(protocol):
            filename = protocol[index]
            protocol = os.path.basename(protocol[index])
        else:
            display('Could not select protocol.')
            return
        protocol = os.path.splitext(protocol)[0]

        self.runNumber = self.exp.timesTriggered
        self.expnameLabel.setText('Exp: <b> {0} </b> - <b> run{1:02d}_{2} </b>'.format(self.expname,self.runNumber,protocol))
        todaydatestr = date.today().strftime('%y%m%d')
        datafolder = '{0}_{1}_{2}_{3}'.format(todaydatestr,self.expname,
                                                self.expprefix.text(),
                                                self.preferences['userPrefix'])
        protocolfolder = 'run{0:02d}_{1}'.format(self.runNumber,protocol)
        logdir = pjoin(datafolder,protocolfolder)
        if 'scanbox' in self.controllerNames:
            if self.controllers['scanbox']['toggle'].isChecked():
                self.controllers['scanbox']['control'].setExperiment(datafolder,
                                                          protocolfolder,
                                                          self.exp.nFramesTotal/self.exp.refreshRate)
        if 'spikeglx' in self.controllerNames:
            if self.controllers['spikeglx']['toggle'].isChecked():
                self.controllers['spikeglx']['control'].setExperiment(datafolder,
                                                                      protocolfolder)
        if ('labcams' in self.controllerNames):
            if (self.controllers['labcams']['toggle'].isChecked()):
                self.controllers['labcams']['control'].setExperiment(datafolder,
                                                                    protocolfolder)
                self.controllers['labcams']['control'].setSave()

        if not self.loggerController is None:
            self.loggerController.startLogging(logdir,self.protocolFilename)
            display(' ---> Run should take {0:.2f} min.'.format((self.exp.nFramesTotal/self.exp.refreshRate)/60.))
        if len(self.controllers):
            display('Blocking for {0} seconds to allow for remotes to sync.'.format(
                waitTime))
            qWait(waitTime*1000)
        
    def buttonStop(self):
        display('Stopping stimuli.')
        # Stop scanbox before
        if 'scanbox' in self.controllerNames:
            if self.controllers['scanbox']['toggle'].isChecked():
                self.controllers['scanbox']['control'].stopAcquisition()
                qWait(500) # Wait a bit to git the network time.
        self.exp.stopTrigger.set()
        # Stop ephys after the experiment to capture all sync pulses
        if 'spikeglx' in self.controllerNames:
            qWait(5000) # Give spikeglx 5 seconds then stop
            if self.controllers['spikeglx']['toggle'].isChecked():
                self.controllers['spikeglx']['control'].stopAcquisition()
                display('Stopping SpikeGLX')
        # Close logging
        #if not self.loggerController is None:
        #    self.loggerController.stopLogging()
        #    qWait(1000)


def main():
    from .utils import getPreferences
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Visual stimulation and experiment control gui.')
    parser.add_argument('expname',metavar='expname',
                        type=str,help='Experiment name (e.g. JC048)')
    parser.add_argument('-u','--user',metavar='user',default='default',
                        type=str,help='User (config name in ~/vstim)')
    parser.add_argument('--due',metavar='dueport',
                        type=str,help='Arduino port',default = [''],
                        nargs=1)
    opts = parser.parse_args()
    due = opts.due[0]
    user = opts.user
    expname = opts.expname

    prefs = getPreferences(user = user)
    app = QApplication(sys.argv)
    display('Starting experiment {0}; user {1}.'.format(expname,user))
    w = VStimGUI(app = app,expname = expname, preferences = prefs)
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
