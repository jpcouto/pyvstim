#!/usr/bin/env python
# Classes for displaying visual stimuli.

from psychopy import visual
from psychopy.visual.windowwarp import Warper
from psychopy import monitors, data, event, core, logging
from psychopy.tools.monitorunittools import pix2deg
from multiprocessing import Event
import time
try:
    from psychopy.info import RunTimeInfo
except:
    pass
import numpy as np
from ..pulseDevices import *

LOGVSTIM = 10

__all__ = ['Experiment']


class Experiment():
    running = False
    keyboard = {}
    def __init__(self,
                 monitor = {'name':'test','distance':20,'rate':30,'screen':0},
                 warp = dict(warp = 'spherical',
                             flipHorizontal=False,
                             flipVertical=False,
                             distance=30,
                             eyepoint=[0.5,0.5]),
                 units = 'deg',
                 logger=None,
                 pulseDevice = None,
                 keyboard = {},
                 progress = True,
                 trigger = None,
                 rig = None,
                 rewardMethod = None,
                 flashIndicator=True,
                 flashIndicatorParameters = None,
                 screenTriggerRig = True,
                 gui = None):
        ''' Usage example:
                exp = experiment()
                # Create stimulus
                stims = [stimulus(exp)]
                # Run the experiment
                exp.run(stims)
                exp.close()
        Joao Couto, January 2016
        '''
        self.monitorPar = monitor
        self.refreshRate = monitor[0]['rate']
        self.size = monitor[0]['sizePix']
        self.units = units
        self.window = []
        self.indicatorClock = core.Clock()
        self.clock = core.Clock()
        self.gui = gui
        # Not allowing different names for now..
        for snum,mon in enumerate(monitor):
            self.window.append(visual.Window(mon['sizePix'],
                                             monitor = mon['name'],
                                             fullscr=mon['fullScreen'],
                                             gamma = mon['gamma'],
                                             units=units,
                                             screen = mon['screen'],
                                             useFBO=True,
                                             winType='pyglet',
                                             color=(0,0,0),
                                             allowGUI=True,
                                             waitBlanking = True,
                                             checkTiming = False,
                                             stereo=False,
                                             allowStencil=True))
            print('''Window {0}:  units: {1} '''.format(snum,units))
        self.warper = []
        for i,window in enumerate(self.window):
            if warp is None:
                self.warper = [None]
            else:
                self.warper.append(Warper(window,
                                          **warp))
                self.warper[i].dist_cm=monitor[i]['distance']
                print('Warper was set on window {0}.'.format(i))
                self.warpProps = warp
            #window.setRecordFrameIntervals(False)
            if 'RunTimeInfo' in dir():
                info  = RunTimeInfo(author = 'mouse',
                                    version = 1.9,
                                    win = window)
                monitor[i]['rate'] = 1000./info['windowRefreshTimeAvg_ms']
            #window._refreshThreshold = 1/float(self.refreshRate)+0.0010 # allow 4ms tolerance
            mon = monitors.Monitor(monitor[i]['name'])
            self.screenSizeDeg = pix2deg(np.array(mon.getSizePix()),mon)
            print('\tScreen is {0:.0f} by {1:.0f} deg.'.format(*self.screenSizeDeg))
            if not self.refreshRate == monitor[i] ['rate']:
                print('Screen rate is: {0}'.format(monitor[0]['rate']))

        self.pulse=None 
        if pulseDevice is None:
            pass
        elif type(pulseDevice) is dict:
            if pulseDevice['name'] == 'parport':
                self.pulse = parallelPortPulse(pulseDevice['address'])
                print('Set parport.')
        else:
            print('Boo... Unknown pulse: ')

        self.stims = None
        self.setKeyActions(keyboard)
        if not progress is None:
            self.pbar = progBar()
        else:
            self.pbar = None
        self.rig = rig
        if not rig is None:
            self.stopTrigger = Event()
            self.trigger = self.rig.due.experiment_running
        else:
            self.stopTrigger = Event()
            self.trigger = Event()
        self.waiting = False
        #self.clearBuffers()
        self.logger = logger
        self.triggerArduino = screenTriggerRig
        self.tStartTriggerArduino = self.getTime()+1000000
        self.flashIndicatorCounter = 0
        self.flashIndicatorParameters = flashIndicatorParameters
        if self.flashIndicatorParameters is None:
            print("Using default indicator parameters!!!")
            self.flashIndicatorParameters = dict()
            self.flashIndicatorParameters['size'] = 8
            self.flashIndicatorParameters['pos'] = -1*self.screenSizeDeg/4 - 8
            self.flashIndicatorParameters['units'] = "deg"
        fIsz = self.flashIndicatorParameters['size']
        fIunits = self.flashIndicatorParameters['units']
        fIpos = self.flashIndicatorParameters['pos']
        self.photosensorIndicator = visual.Circle(self.window[0],
                                                  size=fIsz,
                                                  pos=fIpos,units=fIunits,
                                                  lineColor = None,fillColor=-1)
        self.photosensorIndicatorState = False
        self.flashIndicator = flashIndicator
        if self.flashIndicatorParameters is None:
            self.flashIndicatorParameters = dict()
        if not 'mode' in self.flashIndicatorParameters.keys():
            self.flashIndicatorParameters['mode'] = 0
        if not 'frames' in self.flashIndicatorParameters.keys():
            self.flashIndicatorParameters['frames'] = 20
        self.setIndicator(0)
        self.flipAndWait()
        self.timesTriggered = 0
        

    def setStims(self, stims):
        if not type(stims) == list:
            print('Stims must be a list.')
            return
        self.stims = stims
    
    def stop(self):
        self.stopTrigger.set()
        self.trigger.clear()
        self.running = 0
        print('Setting stop trigger.')
        if not self.rig is None:
            print('Stopping rig.')
            self.rig.stopExperiment()
        if not self.logger is None:
            self.logger.stoplog()

    def setArduinoTriggerTime(self):
        if self.rig is None:
            print('This obviously is not going to work... You need a rig for this...')
        self.tStartTriggerArduino = self.getTime()

    def getTime(self):
        return time.time()
        
    def setTrigger(self):
        if self.rig is None:
            self.trigger.set()
        else:
            print('Starting experiment by arduino.')
            self.rig.startExperiment()
            print('Started experiment with arduino.')
    def setKeyActions(self,keyboard = dict()):
        if not 'q' in keyboard.keys():
            keyboard['q'] = lambda: self.stop()
        if not 'p' in keyboard.keys():
            keyboard['p'] = lambda: self.setTrigger()
        if not 'a' in keyboard.keys():
            self.triggerArduino = True
            keyboard['a'] = lambda: self.setArduinoTriggerTime()
        self.keyboard = keyboard

    def resetClock(self):
        self.clock.reset()
        
    def flipAndWait(self,tflip=None):
        ''' If tflip is None it syncs to the next screen flip. '''
        if tflip is None:
            for w in self.window:                
                w.flip(clearBuffer = True)
            return self.clock.getTime()
        for win in self.window:
            win.flip(False)
        #if tflip > self.clock.getTime():
        #    print('Skipped a frame?')
        #    return -1*self.clock.getTime()
        #print(tflip - self.clock.getTime())
#        cnt = 0
        while (tflip - self.clock.getTime() > 0):
            pass
        return self.clock.getTime()

    def setnFramesTotal(self,N):
        self.nFramesTotal = N
        if not self.pbar is None:
            self.pbar.setTotal(N)
    def setBackground(self,background):
        for window in self.window:
            window.setColor(background)
        self.setIndicator(0)
        self.flipAndWait()

    def flipIndicator(self):
        if self.photosensorIndicatorState:
            self.photosensorIndicator.fillColor = 1
            self.photosensorIndicatorState  = False
        else:
            self.photosensorIndicator.fillColor = -1
            self.photosensorIndicatorState = True
            self.flashIndicatorCounter += 1
        self.photosensorIndicator.draw()
    def setIndicator(self,value):
        if value:
            self.photosensorIndicator.fillColor = 1
            if self.photosensorIndicatorState == True:
                self.indicatorClock.reset()
            self.photosensorIndicatorState  = False
        else:
            self.photosensorIndicator.fillColor = -1
            if self.photosensorIndicatorState == False:
                print('Indicator duration: {0:3.3f}'.format(self.indicatorClock.getTime()))
            self.photosensorIndicatorState = True
            
            self.flashIndicatorCounter += 1
        self.photosensorIndicator.draw()
    def setSessionNumber(self, number):
        self.timesTriggered = number
    
    def processRewardOnLick(self,interval=8):
        '''
        Check if there was a lick this frame.
        If so: Check if the last reward was less than <interval seconds ago>
            If so: give reward and update the time.'''
        if self.rigstatus['licks'][1] > self.nLicksLastReward:
            if (self.currentTime - self.tLastReward) > interval:
                self.rig.giveReward()
                self.tLastReward = self.currentTime
        self.nLicksLastReward = self.rigstatus['licks'][1]

    def processRewardOnPositionAndLick(self, position = [0,1000], interval=8):
        '''
        Check if there was a lick this frame.
            If so: Check if the mouse in the right position:
                If so: Check if the last reward was less than <interval seconds ago>
                    If so: Check if the last reward was less than <interval seconds ago>
                        If so: give reward and update the time.
        '''
        if self.rigstatus['licks'] > self.nLicksLastReward:
            if (self.rigstatus['position'][1] < position[-1]) and (self.rigstatus['position'][1] >= position[0]):
                print(self.tLastReward,self.currentTime)
                if (self.currentTime - self.tLastReward) > interval:
                    self.rig.giveReward()
                    self.tLastReward = self.currentTime
                    print('Reward given.')
        self.nLicksLastReward = self.rigstatus['licks'][0]

    def parseIndicator(self,code,stimcount = 0):
        if self.flashIndicator:
            if self.flashIndicatorParameters['mode'] ==  1:
                # mode 1 flips every 30 frames
                if np.mod(self.nFrames,
                          self.flashIndicatorParameters['frames']) == 0:
                    self.setIndicator(self.photosensorIndicatorState)
                else:
                    self.setIndicator(not self.photosensorIndicatorState)
            elif self.flashIndicatorParameters['mode'] == 2:
                # when the first stimulus changes
                self.setIndicator(np.mod(stimcount,2) == 0)
            else:
                # Mode zero just passes the code from the stimuli
                self.setIndicator(code)
        
    def clearBuffers(self):
        for win in self.window:
            win.setBuffer('left', clear=True)

    def run(self, stims = None, waitForRigPulses = None,
            rewardMethod = None, 
            rewardParameters = dict(interval = 5,
                                    position=[0,300]),
            skipTrigger = False,
            onTriggerFunct = None,
            screenshot_folder = None):
        #self.clearBuffers()
        if self.flashIndicator:
            self.setIndicator(0)
            self.flipAndWait()
        nImagingPulses = 0
        nLicks = 0
        self.currentTime = 0
        self.nLicksLastReward = 0
        self.currentTime = 0
        self.tLastReward = 0
        if rewardMethod is None:
            rewardFunc = lambda: None 
        elif rewardMethod.lower() == 'onlick':
            print('Using processRewardOnLick')
            self.rig.disableRewardOnLap()
            rewardFunc = lambda: self.processRewardOnLick(interval = rewardParameters['interval'])
        elif rewardMethod.lower() == 'onpositionlick':
            print('Using processRewardOnPositionAndLick')
            self.rig.disableRewardOnLap()
            rewardFunc = lambda: self.processRewardOnPositionAndLick(
                position = rewardParameters['position'],
                interval = rewardParameters['interval'])
        if stims is None:
            stims = self.stims
        if stims is None:
            print("Stimulus not defined. Don't know what to do.")
            return
        for s in stims:
            s.iFrame = 0
            s.frameCnt = 0
            s.expcnt = 0
        if not self.pbar is None:
            try:
                self.pbar.reset()
            except:
                print('Failed to reset progress bar.')

        kkeys = self.keyboard.keys()
        self.stopTrigger.clear()
        if not self.pbar is None:
            self.pbar.setValue(0)
            self.setnFramesTotal(self.nFramesTotal)
        if not self.logger is None:
            self.logger.put('# CODES: vstim={0}'.format(LOGVSTIM))
            for s in stims:
                if not self.logger is None:
                    self.logger.put('# VLOG HEADER:'+ s.logHeader + ',indicatorFlag')
        if not self.trigger.is_set():
            print('Waiting for trigger. "p" to manually trigger.')
        self.waiting = True
        while not self.trigger.is_set():
            keyb = event.getKeys(kkeys)
            for k in keyb:
                if k in kkeys:
                    self.keyboard[k]()
            if self.triggerArduino:
                if self.getTime()-self.tStartTriggerArduino > 3:
                    self.setTrigger()
            if skipTrigger:
                self.setTrigger()
            if  self.stopTrigger.is_set():
                self.stop()
                print('Experiment aborted before trigger.')
                return
        print('Experiment triggered.')
        self.waiting = False
        self.flashIndicatorCounter = 0
        self.firstFlipTime = None
        if not onTriggerFunct is None:
            onTriggerFunct()
        if not self.rig is None:
            self.rigstatus = self.rig.getRigStatus()
            if not waitForRigPulses is None:
                while not self.rig.getRigStatus()[waitForRigPulses][1] > 0:
                    if not self.gui is None:
                        self.gui.update()
                self.logger.put('# Vstim trigger on: {0},{1}'.format(
                    waitForRigPulses,
                    self.rig.getRigStatus()[waitForRigPulses][1]))
        if self.trigger.is_set():
            self.timesTriggered += 1
            self.running = 1
        # reset the clock just before starting
        self.nFrames = 0
        self.clock.reset()
        # wait for a bit 5 seconds
        nframestowait = 5*self.refreshRate
        print('Giving 5 seconds blank')
        while nframestowait:
            if self.flashIndicator:
                self.setIndicator(0)
            self.flipAndWait()
            nframestowait -= 1
        # run the experiment
        while self.running and not self.stopTrigger.is_set():
            #self.clearBuffers()
            # Handle key events.
            keyb = event.getKeys(kkeys)
            for k in keyb:
                if k in kkeys:
                    self.keyboard[k]()
            # Handle stimuli
            code = 0
            for stim in stims:
                c,toLog = stim.evolve()
                code += c
                if c < 0:
                    continue
            # Flip and send pulses.
            if code == -1:
                break
            self.parseIndicator(code,stims[0].stimcnt)
            if code > 0 and not self.pulse is None:
                self.window[0].callOnFlip(self.pulse.pulse)
            
            flipTime = self.flipAndWait() #float(self.nFrames + 1)/self.refreshRate)
            #print(1./(flipTime - self.currentTime))
            if not self.rig is None:
                self.rigstatus = self.rig.getRigStatus()
            if not self.logger is None:
                self.logger.put([LOGVSTIM,flipTime*1000.]+
                                toLog +
                                [int(not self.photosensorIndicatorState)])
            if not self.pbar is None:
                self.pbar.update(1)
            if not self.gui is None:
                self.gui.update()
            rewardFunc()
            self.nFrames += 1
            self.currentTime = flipTime 
            if not screenshot_folder is None:
                for iwindow,window in enumerate(self.window):
                    window.getMovieFrame()
                    # todo: make this save to the protocols folder instead of pwd..
                    print('{0}/window{1}_frame_{2:06d}.png'.format(
                        screenshot_folder,
                        iwindow,
                    self.nFrames))
                    window.saveMovieFrames('{0}/window{1}_frame_{2:06d}.png'.format(
                        screenshot_folder,
                        iwindow,
                        self.nFrames))
                self.clearBuffers()
        # wait for a bit 5 seconds
        nframestowait = 5*self.refreshRate
        print('Giving 5 seconds blank')
        while nframestowait:
            if self.flashIndicator:
                # Keep the same state
                self.photosensorIndicator.draw()
            self.flipAndWait()
            nframestowait -= 1
        if not self.logger is None:
            self.logger.put('# END OF VSTIM')
        print('Ran {0} frames in {1} sec [expected: {2}].'.format(
            self.nFrames,
            self.currentTime,
            self.nFrames/self.refreshRate))
        self.stop()
        
    def close(self,quit = True):
        self.stop()
        time.sleep(0.5)
        if not self.logger is None:
            self.logger.stoplog()
        if not self.rig is None:
            self.rig.close()
            time.sleep(1)
        print('Closed window.')
        for win in self.window:
            win.close()
            del win
        del self.window
        if quit:
            core.quit()

def main():
    # Usage example
    import numpy as np
    monname = 'lg-55ec930v'
    mondist = 30.
    monrate = 60.

    # configure monitor in an ipython shell:
    # from psychopy import visual, monitors, info
    # mon = monitors.Monitor('test')
    # mon.setWidth(55.88)
    # mon.setSizePix((1920,1200))
    # mon.saveMon()
    # Configure monitor (lg)
    mon = monitors.Monitor(monname)
    mon.setWidth(121.0)
    mon.setSizePix((1920,1080))
    mon.setGamma(2.0)
    mon.setDistance(mondist)
    mon.saveMon()
#    logfile = "E:\\data\\vstim_presentation\\presentation\\160215_JC016_whisper\\"
#    from glob import glob
#    files = glob(logfile+'\\*.vstimlog')
#    logfile += "mouseControlledGrating_{0:04d}.vstimlog".format(len(files))
    pport = None #{'name':'parport','address' :0xC0A0}
    
    exp = Experiment(size=mon.getSizePix(), fullscr=True, monitor = {'name':monname,
                                                                    'distance':mondist,
                                                                     'rate':monrate,
                                                                     'screen':1},
                     pulseDevice=pport)
    stims = []
    stims.append(fullFieldGrating(exp,tf=[2],sf=[0.2],ori=[0,45,90],
                                  dur=1,tex='sin',units='deg'))
#    stims.append(imageSequence(exp,3,60*5))
#   stims.append(fullFieldGrating(exp,tf=[2],sf=[0.02],ori=[0,90,120],dur=6,tex='sin'))
#    stims.append(mouseControlledGrating(exp))
#    stim = stims[0]
#    keyboard = {'d':lambda : stim.changeTF(0.2),
#                'c':lambda : stim.changeTF(-0.2),
#                'g':lambda : stim.changeSF(0.01),
#                'b':lambda : stim.changeSF(-0.01),
#                'f':lambda : stim.changeORI(20),
#                'v':lambda : stim.changeORI(-20),
#                's':lambda : stim.changeSIZE(5),
#                'x':lambda : stim.changeSIZE(-5),
#                'a':lambda : stim.toggleBlink(1)}
#    exp.setKeyActions(keyboard)
    exp.run(stims)
    exp.close()
    sys.exit()

if __name__ == '__main__':
    main()
