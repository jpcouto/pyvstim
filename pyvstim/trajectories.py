import numpy as np

__all__ = ['circle','super_ellipse','trajectory','loop']

def loop(dat,n=1):
    return np.hstack([dat]*n)

def circle(n = 1000,
           radius = 40,
           start=0.,
           end=360.,
           pos = [0,0],
           output = None,
           **kwargs):
    thetas = np.linspace(start,end,int(n))
    x = radius*np.cos(np.deg2rad(thetas)) + pos[0]
    y = radius*np.sin(np.deg2rad(thetas)) + pos[1]
    if output is None:
        return np.vstack([x,y]).T
    if output == 'x':
        return x
    if output == 'y':
        return y
    
def super_ellipse(n = 1000,
                  width = 80,
                  height = None,
                  start = 0,
                  end = 360,
                  order = 2.5,
                  pos = [0,0],
                  output = None,
                  **kwargs):
    cx,cy = pos
    
    pnts = []
    power = 2.0 / order
    
    thetas = np.deg2rad(np.linspace(start,end,int(n)))
    if height is None:
        height = width
    radiusx = width
    radiusy = height
    
    def signed_power(x,y):
        return np.copysign( np.power(np.fabs(x),y) ,x)
    
    x = cx + radiusx * signed_power(np.cos(thetas), power)
    y = cy + radiusy * signed_power(np.sin(thetas), power)
    
    if output is None:
        return np.vstack([x,y]).T
    if output == 'x':
        return x
    if output == 'y':
        return y

def trajectory(n = 1000,
               start = -80,
               end = 80,
               pos = [0,0],
               rotation = 0,
               output = None,
               **kwargs):
    x = np.linspace(start,end,int(n))
    y = np.zeros_like(x)
    
    angle = np.exp(np.deg2rad(rotation)*1j)
    
    ox,oy = pos
    px = ox+1j*oy
    c=x+1j*y

    c = angle*(c) + px
    
    x = c.real
    y = c.imag
    if output is None:
        return np.vstack([x,y]).T
    if output == 'x':
        return x
    if output == 'y':
        return y
