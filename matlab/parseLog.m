function [logoutdict,comments] = parseLog(filename)

%%% read the file as lines
rawdata = fileread(filename);

logdata=textscan(rawdata,'%s','delimiter','\');
logdata = logdata{1};


%%% Get the comments:
%versionheader = '# Version';
%a= logdata(cellfun(@length,strfind(logdata,versionheader ))==1);
parts=strsplit(logdata{1},' ');
version=str2double(parts{end});


%%% Get the comments:
codeheader = '# CODES:';
codes = logdata(cellfun(@length,strfind(logdata,codeheader))==1);

commentidx = cellfun(@length,strfind(logdata,'#'));
comments=logdata(commentidx==1);

%%% parse codes and build code_dict
codesdict=struct;
nDict=0;
for iCode=1:length(codes)
    line=codes{iCode};
    parts=strsplit(line,{' ',','});
    for iPart=1:length(parts)
        if ~isempty(strfind(parts{iPart},'='))
            S=strsplit(parts{iPart},'=');
            nDict=nDict+1;
            codesdict(nDict).key=str2double(S{2});
            codesdict(nDict).value=S{1};
        end
    end
end
field_names={codesdict.value};
key_vector=cat(2,codesdict.key);
nFields=length(field_names);

% fix brackets (MATLAB suuuuucks)
% needs matlab 2017a
% for c = unique(extractBetween(rawdata,'[',']'))'
%     val   = str2num(c{1});
%     rawdata = strrep(rawdata,['[',c{1},']'],num2str(val(1)));
% end

% low-fi fallback
if ~isempty(strfind(rawdata,'['))
    % just remove brackets and read an extra column
    rawdata=strrep(rawdata,'[','');
    rawdata=strrep(rawdata,']','');
    nCols=14;
else
    nCols=13;
end

%%% read numeric data pulse data first, that's 4 numbers
format=repmat('%f ',1,nCols);
a=textscan(rawdata,format,'delimiter',',','CommentStyle','#','collectoutput',1);

logdata=a{1};
logoutdict=struct;
for iField=1:nFields
    sel=logdata(:,1)==key_vector(iField);
    
    data=logdata(sel,:);
    if size(data,1)==0
        out=[];
    else
        out=data(:,~isnan(data(1,:)));        
    end
    logoutdict.(field_names{iField})=out;
end


%logoutdict
%unique(logoutdict.vstim(:,end))


% tic
% %%% parse inputs
% if nargin>=1&&~isempty(varargin{1})
%     filename = varargin{1};
% else
%     %filename = 'Q:\presentation\171004_JC046_2P_JC\run00_orientation_8dir.log';
%     filename='J:\data\presentation\171006_JC048_2P_V1_BV\run02_RandShapes.log';
% end
% 
% %%% read the file as lines
% fid = fopen(filename);
% logdata=textscan(fid,'%s','delimiter','\');
% logdata = logdata{1};
% fclose(fid);
% 
% %%% Get the comments:
% %versionheader = '# Version';
% %a= logdata(cellfun(@length,strfind(logdata,versionheader ))==1);
% parts=strsplit(logdata{1},' ');
% version=str2double(parts{end});
% 
% keyboard
% 
% %%% Get the comments:
% codeheader = '# CODES:';
% codes = logdata(cellfun(@length,strfind(logdata,codeheader))==1);
% 
% commentidx = cellfun(@length,strfind(logdata,'#'));
% comments=logdata(commentidx==1);
% 
% %% parse codes and build code_dict
% codesdict=struct;
% nDict=0;
% for iCode=1:length(codes)
%     line=codes{iCode};
%     parts=strsplit(line,{' ',','});
%     for iPart=1:length(parts)
%         if ~isempty(strfind(parts{iPart},'='))
%             S=strsplit(parts{iPart},'=');
%             nDict=nDict+1;
%             codesdict(nDict).key=str2double(S{2});
%             codesdict(nDict).value=S{1};
%         end
%     end
% end
% field_names={codesdict.value};
% key_vector=cat(2,codesdict.key);
% nFields=length(field_names);
% 
% %% read numeric data
% fid=fopen(filename,'r');
% switch version
%     case 1.3
%         format=repmat('%f ',1,13);
%     otherwise
%         format=repmat('%f ',1,13);
% end
% %format='%f %f %f %f %f %f %f %f %f %f %f %f %f %f'
% a=textscan(fid,format,'delimiter',',','CommentStyle','#','collectoutput',1);
% fclose(fid);
% 
% logdata=a{1}
% size(logdata)
% %%
% 
% %%% put data in logoutdict
% logoutdict=struct;
% for iField=1:nFields
%     %%
%     sel=logdata(:,1)==key_vector(iField);
%    
%     data=logdata(sel,:);
%     if size(data,1)==0
%         out=[];
%     else
%         out=data(:,~any(isnan(data)));        
%         out(:,1)=find(sel);
%     end
%     logoutdict.(field_names{iField})=out;
% end
% 
% toc
