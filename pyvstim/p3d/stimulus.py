from panda3d.core import Texture, CardMaker, TextureStage,VBase4,FisheyeMaker
from panda3d.core import WindowProperties, ColorBlendAttrib, TransformState
from direct.showbase import ShowBaseGlobal  #global vars defined by p3d
from direct.task import Task
import numpy as np

def pos2card(pos,size):
    x,y = pos
    s = size
    hlf = s/2
    return [x-hlf,x+hlf,y - hlf,y + hlf]
    

class Stimulus2D():
    def __init__(self,experiment,imarray,
                 units = 'norm',
                 pos = [0,0],
                 size = 1.5,
                 angle = [0],
                 mask = None):
        self.exp = experiment
        self.angle = angle[0]
        self.nBlankFrames = 20
        self.pos = pos
        self.size = size
        self.mask = mask
        self.frameCounter = 0
        self.iFrame = -1
        self.iBlank = 0
        self.iTrial = 0
        self.imarray = imarray
        
        self.texture_component_type = Texture.T_unsigned_byte
        self.texture_format = Texture.F_luminance #grayscale

        self.textures = [self.create_texture(im) for im in self.imarray]
        self.blank = self.create_texture(np.zeros([2,2],dtype = 'uint8')+128)
        self.nFrames = len(self.textures)        
        self.textureStage = TextureStage("ts")
        #Create scenegraph
        cm = CardMaker('card01')
        #cm = FisheyeMaker('card01')
        #cm.set_fov(359)
        if not size is None:
            cm.setFrame(*pos2card(pos,size))
        else:
            cm.setFrameFullscreenQuad()
        self.card1 = self.exp.aspect2d.attachNewNode(cm.generate())  
        if not self.mask is None:
            self.card1.setTransparency(TransparencyAttrib.MAlpha, 1)
        #Transform the model(s)
        self.card1.setScale(1)
        self.card1.setR(self.angle)
        # add evolve
        taskMgr.add(self.task_evolve_infinite,'stim_evolve')
    
    def create_texture(self,img,texture_mode=Texture.WM_clamp):
        texture = Texture("stimulus2d")
        texture.setup2dTexture(img.shape[0], 
                               img.shape[0], 
                               self.texture_component_type,
                               self.texture_format)
        texture.setRamImageAs(img, "L")
        texture.setWrapU(texture_mode)
        return texture

    def set_texture(self,itex):
        self.card1.setTexture(self.textureStage, self.textures[itex])  #ts, tx

    def set_blank(self):
        self.card1.setTexture(self.textureStage, self.blank)  #ts, tx

    def task_evolve_infinite(self,task):
        self.frameCounter += 1
        if self.iFrame == -1:
            if self.iBlank < self.nBlankFrames:
                self.set_blank()
                self.iBlank += 1
            else:
                self.iFrame = 0
                self.iBlank = 0
                self.iTrial += 1
        if self.iFrame > -1:
            self.set_texture(self.iFrame)
            self.iFrame += 1
        if self.iFrame == self.nFrames:
            self.iFrame = -1
        return Task.cont
