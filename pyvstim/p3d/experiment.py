# workaround for full screen on secondary display
from panda3d.core import *
loadPrcFileData('setup', 'undecorated 1')

from direct.showbase.ShowBase import ShowBase
from panda3d.core import Texture, CardMaker, TextureStage,VBase4
from panda3d.core import WindowProperties, ColorBlendAttrib, TransformState
from direct.showbase import ShowBaseGlobal  #global vars defined by p3d
from direct.task import Task

dummymonitor = {'name':'test',
                'fullscreen':False,
                'sizePix':[800,800],
                'distance':20,
                'rate':60,
                'screen':0}

class Experiment(ShowBase):
        def __init__(self,
                     monitor = [dummymonitor],
                     units = 'deg',
                     logger=None,
                     keyboard = {},
                     trigger = None,
                     rig = None,
                     rewardMethod = None,
                     flashIndicator=True,
                     flashIndicatorParameters = None,
                     screenTriggerRig = True,
                     gui = None,
                     noMouse = True):
            ''' Usage example:
    exp = Experiment()
    stim = Stimulus2D(exp,r)
    exp.run()
    exp.close()
            Joao Couto, April 2019
            '''
            super(Experiment,self).__init__()
            self.monitorPar = monitor
            self.refreshRate = monitor[0]['rate']
            self.size = monitor[0]['sizePix']
            self.window = []
            self.indicatorClock = None
            self.clock = None
            self.gui = gui
            self.setFrameRateMeter(True)
            self.setBackgroundColor(.5,.5,.5)
            
            self.window_properties = WindowProperties()
            self.window_properties.setSize(*self.size)
            self.window_properties.setFixedSize(True)
#            self.window_properties.setOrigin(1920,0)
            self.window_properties.setFullscreen(self.monitorPar[0]['fullscreen'])
            self.window_properties.setTitle("Experiment")
            self.window_properties.setCursorHidden(True)
            ShowBaseGlobal.base.win.requestProperties(self.window_properties)

            self.initkeys()
            # Draw some frames
            [self.step() for i in range(10)]
        def initkeys(self):
            self.accept('q', self.stop)
        def stop(self):
            taskMgr.stop()
        def close(self):
            self.close_window(self.win)
            self.destroy()
        def step(self):
            taskMgr.step()

def main():
        from pyvstim.p3d.stimulus import Stimulus2D
        from pyvstim.stimgen import bandpass_noise
        import numpy as np
        r = bandpass_noise(npix = 64,
                           contrast = 1,
                           stimlen = 4*60,
                           width=50,
                           sf_0 = 0.1,
                           tf_0 = 1)
        exp = Experiment(monitor = [dummymonitor])
        stim = Stimulus2D(exp,r,size = 2.0)
        y = r.astype(np.float32)-128
        contrast = 3
        y = np.clip(y*contrast+128,0,255).astype(np.uint8)
        stim2 = Stimulus2D(exp,y,
                           size = 0.5,
                           pos = (-.2,-.5),angle = [20])
        exp.run()
        exp.close()

if __name__ == '__main__':
        main()
