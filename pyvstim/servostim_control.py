#! /usr/bin/env python
# Class to interface with the arduino that controls the servo and with the thorlabs controller.
#
import serial
import time
import sys
import numpy as np

import os
from os.path import join as pjoin
# ARDUINO MESSAGES
ERROR='E'
NOW = 'N'
FLIP = 'F'
STX='@'
SEP = '_'
ETX=serial.LF.decode()
ARM = 'A'
SET_PARAMETERS = 'P'

class ServoStageControl():
    def __init__(self,duinoport,stage_number=0,
                 baudrate=115200,
                 acceleration = 3,
                 velocity = 4,
                 timeout=0.03):
        print("[ServoStage] - Loading thorlabs dll... (this takes a while)")
        from thorlabs_apt import list_available_devices,Motor
        device = list_available_devices()[stage_number][1]
        self.motor = Motor(device)
        self.zero_position = self.motor.position
        print("[ServoStage] - Connected to thorlabs stage {0}".format(device))
        self.set_speed(acceleration,velocity)
        self.motor.move_to(0)        
        self.ino = serial.Serial(port=duinoport,
                                 baudrate=baudrate,
                                 timeout = timeout)
        print("[ServoStage] - Connected to servo arduino on {0}.".format(duinoport))
    def servo_act(self):
        self.ino.write((STX+NOW+ETX).encode())

    def servo_flip(self):
        self.ino.write((STX+FLIP+ETX).encode())
    
    def servo_set_duration(self,duration=1):
        self.ino.write((STX+SET_PARAMETERS+SEP+str(int(duration*1000))+ETX).encode())
    def set_speed(self,accel,vel):
        self.acceleration = accel
        self.velocity = vel
        print("[ServoStage] - Motor acceleration {0} velocity - {1}.".format(accel,vel))
        self.motor.set_velocity_parameters(0,self.acceleration,self.velocity)
    def arm(self,position=0):
        self.move_to(position)
        self.ino.write((STX+ARM+ETX).encode())

    def move_to(self,position=0):
        self.motor.move_to(position)
