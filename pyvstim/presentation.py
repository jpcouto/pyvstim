from .utils import parseProtocolFile
import os
import numpy as np
from os.path import join as pjoin
from .loggers import Logger
from .trajectories import *
from multiprocessing import Queue
import pandas as pd


pd.options.mode.chained_assignment = None

def parseStimOptDefaults(stimopt,stimpar,exp,prefs = None):
    if not 'Background' in stimopt.keys():
        stimopt['Background'] = 0
    if not 'Mask' in stimopt.keys():
        stimopt['Mask'] = None
    
    if not 'mask_scale' in stimpar.columns:
        stimopt['MaskScale'] = None
    else:
        stimopt['MaskScale'] = np.array(stimpar['mask_scale'])
    if not 'Interpolate' in stimopt:
        stimopt['Interpolate'] = True
    if not type(stimopt['Interpolate']) is bool:
        if stimopt['Interpolate'].strip(' ') in ['1','true','True']:
            stimopt['Interpolate'] = True
        else:
            stimopt['Interpolate'] = False
    if 'mask' in stimpar.columns:
        masks = np.array(stimpar['mask'])
        for i,mask in enumerate(masks):
            if not mask in ['sqr','circle','raisedCos']:
                if 'np.' in mask:
                    # Support numpy stuff/array
                    masks[i] = eval(mask)
                else:
                    # Then it must be an image; append the image path.
                    image_path = pjoin(prefs['stimsFolder'],mask)
                    convert_im=True
                    if not os.path.isfile(image_path):
                        raise(ValueError('Could not find file: {0}, {1} - {2}'.format(iStim,iFrame,image_path)))   
                    else:
                        if convert_im:
                            # handle diverse input images and generate valid alpha mask
                            from PIL import Image
                            from cv2 import resize as imresize
                            im = np.array(Image.open(image_path),dtype=float)                                                        
                            
                            if not stimopt['MaskScale'] is None:
                                scaling_factor=float(stimopt['MaskScale'][i])
                                im=imresize(im,None,fx=scaling_factor,fy = scaling_factor)

                            # convert bg into gray and figure into see-through
                            bg_value = im[0,0] # pixel 1,1 will have background color - those will be masked
                            mask = im!=bg_value # what is not bg will                                            
                            mask = mask*2.-1

                            # return to natural orientation
                            mask = np.flipud(mask) # or use flipvertical in warp settings
                            
                            # fit in screen rect                        
                            bg_im=np.zeros((1024,1024))-1
                            bg_size=bg_im.shape
                            mask_size=mask.shape
                            bg_im[bg_size[0]/2-mask_size[0]/2:bg_size[0]/2+mask_size[0]/2 , bg_size[1]/2-mask_size[1]/2:bg_size[1]/2+mask_size[1]/2]=mask

                            # append to list
                            masks[i] = bg_im
                        else:
                            masks[i] = image_path
                    
        stimopt['Mask'] = masks
    if not 'c' in stimpar.columns:
        stimpar.insert(loc=len(stimpar.columns),column='c',value = np.ones(len(stimpar,)))
    if not 'Shuffle' in stimopt.keys():
        stimopt['Shuffle'] = False
    if not type(stimopt['Shuffle']) is bool:
        if stimopt['Shuffle'].strip(' ') in ['1','true','True']:
            stimopt['Shuffle'] = True
        else:
            stimopt['Shuffle'] = False
    if not 'ApertureMask' in stimopt.keys():
        stimopt['ApertureMask'] = None
    if not 'GroupStimuliTrials' in stimopt.keys():
        stimopt['GroupStimuliTrials'] = False
    if not 'BlankDuration' in stimopt.keys():
        stimopt['BlankDuration'] = 0
    if not 'Texture' in stimopt.keys():
        stimopt['Texture'] = 'sqr' # sin
    if 'Texture' in stimopt.keys():
        if 'np.' in stimopt['Texture']:
            stimopt['Texture'] = eval(stimopt['Texture'])
    if not 'Units' in stimopt.keys():
        stimopt['Units'] = 'deg'
    if not 'nTrials' in stimopt.keys():
        stimopt['nTrials'] = 8
    # Stimulus position and size
    if 'xc' in stimpar.columns:
        for i,s in enumerate(stimpar.xc):
            if type(s) is str:
                try:
                    stimpar.xc.loc[i] = eval(s)
                except:
                    print('Failed to evaluate ' + s)
                    import ipdb
                    ipdb.set_trace()
                    raise
    else:
        stimpar.insert(loc=len(stimpar.columns),column='xc',value = np.zeros(len(stimpar,)))
    if 'yc' in stimpar.columns:
        for i,s in enumerate(stimpar.yc):
            if type(s) is str:
                stimpar.yc.loc[i] = eval(s)
    else:
        stimpar.insert(loc=len(stimpar.columns),column='yc',value = np.zeros(len(stimpar,))) 

    for i,s in enumerate(stimpar.xc):
        if type(s) is str:
            stimpar.xc.loc[i] = eval(s)
    for i,s in enumerate(stimpar.yc):
        if type(s) is str:
            stimpar.yc.loc[i] = eval(s)
    if 'xpos' in stimopt.keys():
        stimopt['xpos'] = [v + eval(stimopt['xpos']) for v in stimpar.xc]
    else:
        stimopt['xpos'] = stimpar.xc
    if 'ypos' in stimopt.keys():
        stimopt['ypos'] = [v + eval(stimopt['ypos']) for v in stimpar.yc]
    else:
        stimopt['ypos'] = stimpar.yc    
    # Aperture position and size
    if not stimopt['ApertureMask'] is None:
        for i,s in enumerate(stimpar.apxc):
            if type(s) is str:
                stimpar.apxc.loc[i] = eval(s)
        for i,s in enumerate(stimpar.apyc):
            if type(s) is str:
                stimpar.apyc.loc[i] = eval(s)
        if 'apypos' in stimopt.keys():
            stimopt['apypos'] = [v + eval(stimopt['apypos']) for v in stimpar.apyc]
        else:
            stimopt['apypos'] = stimpar.apyc
        if 'apxpos' in stimopt.keys():
            stimopt['apxpos'] = [v + eval(stimopt['apxpos']) for v in stimpar.apxc]
        else:
            stimopt['apxpos'] = stimpar.apxc
    for k in stimpar.keys():
        for i,s in enumerate(stimpar[k]):
            if type(s) is str:
                if 'np.' in s:
                    print('Evaluating' + k)
                    stimpar[k].iloc[i] = eval(s)

    return stimopt,stimpar

def processGratingsStimuliFromProt(stimopt,stimpar,exp,prefs):
    stimopt,stimpar = parseStimOptDefaults(stimopt,stimpar,exp,prefs)
    print('Processing gratings.')
    size = np.vstack([stimpar.width,
                      stimpar.height]).T

    if not stimopt['ApertureMask'] is None:
        apsize = np.vstack([stimpar.apwidth,
                        stimpar.apheight]).T
        aperture = dict(mask = stimopt['ApertureMask'],
                        x = stimopt['apxpos'],
                        y = stimopt['apypos'],
                        size = apsize)
    else:
        aperture = None
    if 'n' in stimpar.columns:
        iStims = np.array(stimpar.n,dtype = int)
    else:
        iStims = None
    stimdict = dict(experiment = exp,
                    iWindow=0,
                    tf=[t for t in stimpar.drift],
                    sf=[s for s in stimpar.sf],
                    iStims = iStims,
                    ori=[o for o in stimpar.ori],
                    dur=[d for d in stimpar.dur],
                    contrast = [c for c in stimpar.c],
                    size = [s for s in size],
                    x = [x for x in stimopt['xpos']],
                    y = [y for y in stimopt['ypos']],
                    mask=stimopt['Mask'],
                    tex=stimopt['Texture'],
                    aperture = aperture,
                    units=stimopt['Units'],
                    reps = int(stimopt['nTrials']),
                    shuffle = stimopt['Shuffle'],
                    groupStimuliTrials = bool(stimopt['GroupStimuliTrials']),
                    background = float(stimopt['Background']),
                    blankDur=float(stimopt['BlankDuration']))
    return stimdict

def processActuatorGratingsStimuliFromProt(stimopt,stimpar,exp,prefs):
    stimdict = processGratingsStimuliFromProt(stimopt,stimpar,exp,prefs)

    if not 'PuffFrequency' in stimopt.keys():
        stimopt['PuffFrequency'] = 10
    else:
        stimopt['PuffFrequency'] = float(stimopt['PuffFrequency'])
    if not 'PuffWidth' in stimopt.keys():
        stimopt['PuffWidth'] = 0.01
    else:
        stimopt['PuffWidth'] = float(stimopt['PuffWidth'])
    if 'puff' in stimpar.columns:
        puffStims = np.array(stimpar.puff)
    else:
        puffStims = np.ones(len(stimpar))
    if 'actuator' in stimpar.columns:
        actuator = np.array(stimpar.actuator)
    else:
        actuator = np.zeros(len(stimpar))
    stimdict = dict(stimdict,
                    frequency = stimopt['PuffFrequency']*puffStims,
                    actuatorWidth = stimopt['PuffWidth'],
                    actuator = actuator)
    return stimdict

def processShutterStimuliFromProt(stimopt,stimpar,exp,prefs,doubleWidth = True):
    if 'gratings' in stimopt['StimulusType']:
        stimType = 'gratings'
        stimdict = processGratingsStimuliFromProt(stimopt,stimpar,exp,prefs)

    elif 'PicsFolder' in stimopt.keys():
        stimType = 'ImageSequence'
        stimdict = processImageSequenceStimuliFromProt(stimopt,stimpar,exp,prefs,doubleWidth = True)

    if 'actuator' in stimpar.columns:
        actuator = np.array(stimpar.actuator)
    else:
        actuator = np.zeros(len(stimpar))
    if 'shutter' in stimpar.columns:
        shutter = np.array(stimpar.shutter)
    else:
        shutter = np.zeros(len(stimpar))
    stimdict = dict(stimdict,
                    stimType = stimType,
                    actuator = actuator,
                    shutter = shutter)
    return stimdict

def processPMTGatedStimuliFromProt(stimopt,stimpar,exp,prefs,doubleWidth = True):
    #stimopt,stimpar = parseStimOptDefaults(stimopt,stimpar,exp,prefs)
    
    if 'gratings' in stimopt['StimulusType']:
        stimType = 'gratings'
        stimdict = processGratingsStimuliFromProt(stimopt,stimpar,exp,prefs)

    elif 'PicsFolder' in stimopt.keys():
        stimType = 'ImageSequence'
        stimdict = processImageSequenceStimuliFromProt(stimopt,stimpar,exp,prefs,doubleWidth = True)

    if 'act' in stimpar.columns:
        act = np.array(stimpar.act)
    else:
        act = np.zeros(len(stimpar))
    stimdict = dict(stimdict,
                    width = int(stimopt['Width']),
                    margin = int(stimopt['Margin']),
                    npulses = int(stimopt['NPulses']),
                    stimType = stimType,
                    stimflag = act)
    return stimdict

def processServoStageStimuliFromProt(stimopt,stimpar,exp,prefs):
    stimopt,stimpar = parseStimOptDefaults(stimopt,stimpar,exp,prefs)
    if 'n' in stimpar.columns:
        iStims = np.array(stimpar.n,dtype=int)
    else:
        iStims = None
    if 'dur' in stimpar.columns:
        dur = np.array(stimpar.dur)
    else:
        dur = None
    if 'pos' in stimpar.columns:
        pos = np.array(stimpar.pos)
    else:
        pos = None
    if iStims is  None or pos is None or dur is None:
        print('ServoStage needs n,dur and pos')
        raise ValueError
    stimdict = dict(experiment = exp,
                    iWindow=0,
                    duration=[float(d) for d in dur],
                    stage_position = [float(c) for c in pos],
                    shuffle = stimopt['Shuffle'],
                    reps = int(stimopt['nTrials']),
                    background = float(stimopt['Background']),
                    blankDur=float(stimopt['BlankDuration']))
    return stimdict


def processImageSequenceStimuliFromProt(stimopt,stimpar,exp,prefs,doubleWidth = True):
    stimopt,stimpar = parseStimOptDefaults(stimopt,stimpar,exp,prefs)
    if not 'DecimationRatio' in stimopt.keys():
        stimopt['DecimationRatio'] = 1
    if not 'LoopBack' in stimopt.keys():
        stimopt['LoopBack'] = False
    else:
        if stimopt['LoopBack'] in ['true','True','1']:
            stimopt['LoopBack'] = True
            print('LoopBack on.')
        else:
            stimopt['LoopBack'] = False

    if not 'dur' in stimpar.keys():
        if not 'len' in stimpar.keys():
            print('Unknown duration/length... crack...')
            raise
        dur = np.array([float(l)/(exp.refreshRate/float(stimopt['DecimationRatio'])) for l in stimpar['len']])
    else:
        dur = stimpar.dur
    if doubleWidth:
        stimpar.width *= 2
    ori = None
    if 'imageori' in stimpar.keys():
        ori = np.array(stimpar.imageori) 
    if 'height' in stimpar.keys():
        stimpar.height *= 2
        size = np.vstack([stimpar.width,
                          stimpar.height]).T
    else:
        size = stimpar.width
    if not 'PicsNameFormat' in stimopt.keys():
        print('No PicsNameFormat in the protfile')
        raise
    stimframespath = []
    for i,iStim in enumerate(stimpar.n):
        iStim = int(iStim)
        stimframespath.append([])
        nFrames = int(stimpar['len'].iloc[i])
        for iFrame in range(nFrames):
            image_path = pjoin(prefs['stimsFolder'],
                               stimopt['PicsFolder'],
                               stimopt['PicsNameFormat'].format(iStim,iFrame))
            if not os.path.isfile(image_path):
                raise(ValueError('Could not find file: {0}, {1} - {2}'.format(iStim,iFrame,image_path)))
            stimframespath[-1].append(image_path)
    # Aperture position and size
    if not stimopt['ApertureMask'] is None:
        apsize = np.vstack([stimpar.apwidth,
                            stimpar.apheight]).T
        aperture = dict(mask = stimopt['ApertureMask'],
                        x = stimopt['apxpos'],
                        y = stimopt['apypos'],
                        size = apsize)
    else:
        aperture = None
    stimdict = dict(stimopt,experiment = exp,
                    stimFramePaths = stimframespath,
                    dur=dur,
                    contrast = stimpar.c,
                    size = size,
                    ori = ori,
                    x = stimopt['xpos'],
                    y = stimopt['ypos'],
                    mask=stimopt['Mask'],
                    units=stimopt['Units'],
                    reps = int(stimopt['nTrials']),
                    decimationFactor = int(stimopt['DecimationRatio']),
                    groupStimuliTrials = bool(stimopt['GroupStimuliTrials']),
                    blankDur=float(stimopt['BlankDuration']),
                    aperture = aperture,
                    shuffle = stimopt['Shuffle'],
                    interpolate = stimopt['Interpolate'],
                    background = float(stimopt['Background']),
                    loopBack = stimopt['LoopBack'])
    return stimdict

def processActuatorImageSequenceStimuliFromProt(stimopt,stimpar,exp,prefs,doubleWidth = True):
    stimdict = processImageSequenceStimuliFromProt(stimopt,stimpar,exp,prefs)

    if not 'PuffFrequency' in stimopt.keys():
        stimopt['PuffFrequency'] = 0.333
    else:
        stimopt['PuffFrequency'] = float(stimopt['PuffFrequency'])
    if not 'PuffWidth' in stimopt.keys():
        stimopt['PuffWidth'] = 0.005
    else:
        stimopt['PuffWidth'] = float(stimopt['PuffWidth'])
    if 'puff' in stimpar.columns:
        puffStims = np.array(stimpar.puff)
    else:
        puffStims = np.ones(len(stimpar))
    if 'actuator' in stimpar.columns:
        actuator = np.array(stimpar.actuator)
    else:
        actuator = np.zeros(len(stimpar))

    stimdict = dict(stimdict,
                    frequency = stimopt['PuffFrequency']*puffStims,
                    actuator = actuator,
                    actuatorWidth = stimopt['PuffWidth'])

    return stimdict
        
def processClosedLoopActuator(stimopt,stimpar,exp,prefs):
    if not 'BlackLapProbability' in stimopt.keys():
        stimopt['BlackLapProbability'] = 0.8
    from .psych import stimulus as s
    stim = s.ClosedLoopActuator(exp,dur = float(stimopt['Duration']),
                                actuatorPos = eval(stimopt['ActuatorPosition']),
                                actuator = eval(stimopt['Actuator']),
                                actuatorDuration = eval(stimopt['ActuatorDuration']),
                                actuatorFrequency = eval(stimopt['ActuatorFrequency']),
                                actuatorWidth = eval(stimopt['ActuatorWidth']),
                                background = float(stimopt['Background']),
                                blackLapProbability = float(stimopt['BlackLapProbability']))
    return stim

def protfileExperiment(exp, filename = None,prefs = None):
    from .psych import stimulus as s
    stims = []
    stimopt,stimpar = parseProtocolFile(filename)
    if stimopt['StimulusType'] == 'gratings':
        stimdict = processGratingsStimuliFromProt(stimopt,stimpar,exp,prefs)
        stims.append(s.Gratings(**stimdict))
    elif 'shutter' in stimopt['StimulusType'].lower():
        stimdict = processShutterStimuliFromProt(stimopt,stimpar,exp,prefs)
        stims.append(s.Shutter(**stimdict))
    elif 'pmt_gated' in stimopt['StimulusType'].lower():
        stimdict = processPMTGatedStimuliFromProt(stimopt,stimpar,exp,prefs)
        stims.append(s.GatedPMTStim(**stimdict))
    elif 'servostage' in stimopt['StimulusType'].lower():
        stimdict = processServoStageStimuliFromProt(stimopt,stimpar,exp,prefs)
        stims.append(s.ServoStageStim(**stimdict))
    elif stimopt['StimulusType'] == 'puffGratings':
        print('Loading actuator gratings.')
        stimdict = processActuatorGratingsStimuliFromProt(stimopt,stimpar,exp,prefs)
        stims.append(s.ActuatorGratings(**stimdict))
    elif stimopt['StimulusType'] == 'closedLoopActuator':
        stims.append(processClosedLoopActuator(stimopt,stimpar,exp,prefs))
    elif 'PicsFolder' in stimopt.keys():
        if 'puff' in stimopt['StimulusType']:
            stimdict = processActuatorImageSequenceStimuliFromProt(stimopt,stimpar,exp,prefs)
            stims.append(s.ActuatorImageSequence(**stimdict))
        elif 'WheelTask' in stimopt['StimulusType']:
            stimdict = processImageSequenceStimuliFromProt(stimopt,stimpar,exp,prefs)
            stims.append(s.WheelTask(**stimdict))
        else:
            stimdict = processImageSequenceStimuliFromProt(stimopt,stimpar,exp,prefs)
            stims.append(s.ImageSequence(**stimdict))
    else:
        raise(ValueError,'Boo.. you suck!')
    return stims,(stimopt,stimpar)
    
def main():
    from .utils import getPreferences
    from psychopy import monitors
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Run experiment from protocol file.')
    parser.add_argument('filename',metavar='filename',
                        type=str,help='protocol file',
                        nargs=1)
    parser.add_argument('--due',metavar='dueport',
                        type=str,help='Arduino port',default = [''],
                        nargs=1)
    opts = parser.parse_args()
    filename = opts.filename[0]
    due = opts.due[0]
    
    logQ = Queue()
    logger = Logger(inQ = logQ)
    logger.start()
    logger.startlog()
    if len(due):
        from rig import BehaviorRig
        print('Connecting to:  '+due)
        rig = BehaviorRig(port = due,logger = logger)
    else:
        rig = None
    prefs = getPreferences()
    
    mon = monitors.Monitor(prefs['monitor']['name'])
    mon.setWidth(prefs['monitor']['width'])
    mon.setSizePix(prefs['monitor']['sizePix'])
    mon.setGamma(prefs['monitor']['gamma'])
    mon.setDistance(prefs['monitor']['distance'])
    mon.saveMon()

    monitor = {'name':prefs['monitor']['name'],
               'distance':prefs['monitor']['distance'],
               'rate':prefs['monitor']['rate'],
               'screen':prefs['monitor']['screen']}
    parport = dict(name='parport',address=0x0378)
    parport = None
    prefs['warp']['warpfile']=''
    from .psych import Experiment
    exp = Experiment(size=prefs['monitor']['sizePix'],
                     fullscr=prefs['monitor']['fullScreen'],
                     monitor = monitor,
                     logger = logger,
                     pulseDevice=parport,progress=True,
                     units='deg',
                     rig = rig,
                     warp = prefs['warp'])
    stims = protfileExperiment(exp,filename = filename,prefs = prefs)
    exp.setStims(stims)
    exp.run()
    logger.close()
    rig.close()
    exp.close()


if __name__ == '__main__':
    main()
