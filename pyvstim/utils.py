import json
from os.path import join as pjoin
import os
from scipy.interpolate import interp1d
from scipy.stats import mode
import numpy as np
import pandas as pd
import sys
from datetime import datetime,date
try:
    from cStringIO import StringIO
except:
    try:
        from StringIO import StringIO
    except ImportError:
        from io import StringIO

def display(msg):
    sys.stdout.write('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg + '\n')
    sys.stdout.flush()



def parseVStimLog(fname):
    comments = []
    with open(fname,'r') as fd:
        for line in fd:
            if line.startswith('#'):
                comments.append(line.strip('\n').strip('\r'))
    codes = {}
    vlogheader = []
    righeader = []
    for c in comments:
        if c.startswith('# CODES:'):
            cod = c.strip('# CODES:').strip(' ').split(',')
            for cd in cod:
                k,v = cd.split('=')
                codes[int(v)] = k
        elif c.startswith('# VLOG HEADER:'):
            cod = c.strip('# VLOG HEADER:').strip(' ').split(',')
            vlogheader = [c.replace(' ','') for c in cod]
        elif c.startswith('# RIG CSV:'):
            cod = c.strip('# RIG CSV:').strip(' ').split(',')
            righeader = [c.replace(' ','') for c in cod]

    logdata = pd.read_csv(fname,
                          names = [i for i in range(len(vlogheader))],
                          delimiter=',',
                          header=None,comment='#',engine='c')
    data = dict()
    for v in codes.keys():
        k = codes[v]
        data[k] = logdata[logdata[0]==v]
        
        if len(data[k]):
            tmp = data[k].iloc[0].copy()
            ii = np.where([type(t) is str for t in tmp])
            for i in ii:
                tmp[i] = 0
            idx = np.where([~np.isnan(d) for d in tmp])[0]
            data[k] = data[k][idx]
            if len(idx) <= len(righeader):
                cols = righeader
            else:
                cols = vlogheader[:len(idx)]
            data[k] = pd.DataFrame(data = data[k])
            data[k].columns = cols
    if 'vstim' in data.keys() and 'screen' in data.keys():
        # extrapolate duinotime from screen indicator
        indkey = 'not found'
        fliploc = []
        if 'indicatorFlag' in data['vstim'].keys():
            indkey = 'indicatorFlag'
            fliploc = np.where(np.diff(np.hstack([0,
                                                  data['vstim']['indicatorFlag'],
                                                  0]))!=0)[0]
        elif 'blank' in data['vstim'].keys():
            indkey = 'blank'
            fliploc = np.where(np.diff(np.hstack([0,
                                                  data['vstim']['blank']==0,
                                                  0]))!=0)[0]
        if len(data['screen'])==len(fliploc):
            data['vstim']['duinotime'] = interp1d(
                fliploc,
                data['screen']['duinotime'],
                fill_value="extrapolate")(
                    np.arange(len(data['vstim'])))
        else:
            
            print(
                'The number of screen pulses {0} does not match the visual stimulation {1}:{2} log.'
                  .format(len(data['screen']),indkey,len(fliploc)))
    return data,comments

pd.options.mode.chained_assignment = None

def parseProtocolFile(protfile):
    options = {}
    with open(protfile,'r') as fid:
        string = fid.read().split('\n')
        for i,s in enumerate(string):
            tmp = s.split('=')
            tmp = [t.strip(' ') for t in tmp]
            # Because the first lines are always like this...
            if len(tmp)>1:
                options[tmp[0]] = tmp[1].replace('\r','')
                try:
                    prot[tmp[0]] = int(prot[tmp[0]])
                except:
                    try:
                        prot[tmp[0]] = float(prot[tmp[0]])
                    except:
                        pass
            else:
                break
        tmp = string[i::]
        tmp = [t.replace('\r','').replace('\t',' ').strip().split() for t in tmp]
        tmp = [','.join(t) for t in tmp ]
        try:
            params = pd.read_csv(StringIO(u"\n".join(tmp)),
                                 index_col=None)
        except pd.io.common.EmptyDataError:
            params = None
    return options,params


def getCommitHash():
    import subprocess as sub
    codedir = os.path.dirname(os.path.abspath(__file__))
    try:
        output = sub.check_output(['git','rev-parse',
                                   '--verify','HEAD',
                                   '--short'],cwd = codedir)
        commit_ref = output.strip(b'\n').decode()
    except sub.CalledProcessError:
        print('''
        +++++++++++++++++++++++++++++++++++
        IMPORTANT PYVSTIM WARNING: 
        +++++++++++++++++++++++++++++++++++
    Could not fetch git commit hash!!! 
    You won't be able to retrieve the code version.
        
        Make sure to clone and install with "python setup.py develop"

    Don't run experiments like this!
        ++++++++++++++++++++++++++++++++++
    ''')
        commit_ref = None
    return commit_ref

PYVSTIM_CODE_VERSION = getCommitHash()


def getStimuliTimesFromLog(logfname,log=None):
    '''
    stimtimes,stimpars,stimoptions = getStimuliTimesFromLog(fname)
    Needs access to both the log and the prot file (under the same name).
    stimtimes is a 4 column matrix: iStim,iTrial,starttime,endtime
    '''
    if log is None:
        log,comm = parseVStimLog(logfname)
    stimoptions,stimpars = parseProtocolFile(logfname.replace('.log','.prot'))
    # Figure out where the stim happened
    #ustims = log['vstim'].iStim*(1-log['vstim'].blank)
    #diffs = np.diff(np.hstack([0,ustims,0]))
    #idxonsets = np.where(diffs > 0)[0]
    #idxoffsets = np.where(diffs < 0)[0]
    #stimtimes = np.vstack([log['vstim'][timekey].iloc[idxonsets],
    #                       log['vstim'][timekey].iloc[idxoffsets]]).T/1000.
    #stimidx = idxonsets

    timekey = 'presentTime'
    if 'duinotime' in log['vstim'].keys():
        timekey = 'duinotime'
    else:
        print('Using vstim times (no duino).')
    if 0 in np.unique(np.array(log['vstim'].iStim)):
        # Increment by one because of multiplication
        log['vstim'].iStim += 1.
    if not ('iStim' in log['vstim'].keys() and
            'iTrial' in log['vstim'].keys() and
            'blank' in log['vstim'].keys()):
        print('Did not get stimtimes (iStim,iTrial,blank info needed).')
        return None,stimpars,stimoptions
    ustims = np.array(log['vstim'].iStim*(1.-log['vstim'].blank))
    utrials = np.array(log['vstim'].iTrial*(1.-log['vstim'].blank))
    unique_reps = log['vstim'][['iStim','iTrial']].drop_duplicates()
    unique_reps = unique_reps.iloc[np.array(unique_reps.iStim)>0]
    stimtimes = np.zeros([len(unique_reps),2])
    stimidx = np.zeros([len(unique_reps)])
    for i in range(len(unique_reps)):
        iStim,iTrial = unique_reps[['iStim','iTrial']].iloc[i]
        idx = np.where((ustims == iStim) & (utrials == iTrial))[0]
        stimtimes[i,:] = log['vstim'][timekey].iloc[[idx[0],idx[-1]]]/1000.
        stimidx[i] = idx[0]
        
    stimsdicts = log['vstim'].iloc[stimidx]
    assert len(stimtimes) == len(stimsdicts), "Logged stims not the same size as screen indicator.[{0},{1}]".format(len(stimtimes),len(stimsdicts))

    if not 'BlankDuration' in stimoptions.keys():
        stimoptions['BlankDuration'] = 0
    if not 'Shuffle' in stimoptions.keys():
        stimoptions['Shuffle'] = False
    #assert len(np.unique(stimsdicts['iStim'])) == len(stimpars), 'Crack.. don t know what to do with stimparameters... check protfile and log'
    # merge duplicated stimuli... just in case these were added for randomization purposes
    ustimcodes = np.sort(np.unique(stimsdicts['iStim']))
    unewstimcodes = ustimcodes.copy()
    unique_stimpars = stimpars.drop_duplicates()
    if 'gratings' in stimoptions['StimulusType'].lower():
        unique_stimpars = stimpars.drop_duplicates('n')
    if len(np.unique(stimsdicts['iStim'])) == len(stimpars):
        for s in range(len(unique_stimpars)):
            idx = np.where(np.array(stimpars == unique_stimpars.iloc[s]).all(axis = 1))[0]
            #idx = np.where(np.sum(stimpars.loc[:, stimpars.columns != 'n'] != unique_stimpars.loc[s, stimpars.columns != 'n'],axis=1) == 0)[0]
            unewstimcodes[idx] = s
    else:
        print('WARNING: Stimuli set has concatenated stims and these might not be handled correctly.')
        for o,n in zip(ustimcodes,unewstimcodes):
            stimsdicts.loc[stimsdicts['iStim'] == o,'iStim'] = -1*n
        stimsdicts.loc[:,'iStim'] *= -1
        stimpars = unique_stimpars
    stimtimes = np.hstack([np.array(stimsdicts[['iStim','iTrial']]),stimtimes])
    return stimtimes,stimpars,stimoptions

def velocityFromPosition(time,position):
    return np.diff(np.append(position,position[-1]))/np.diff(time[:2])

def ticksToPosition(timeticks,posticks,beltLength = 150,samplingRate = 100.):
    # Normalizes to position on belt and resets to lap. 
    position = posticks.astype(np.float32).copy()
    lapidx = np.where(np.diff(position)<-10)[0]
    lapidx = np.insert(lapidx,0,-1)
    averagelap = np.mean(position[lapidx])
    lapcount = 0
    for l in range(len(lapidx)-1):
        position[lapidx[l]+1:lapidx[l+1]+1] /= position[lapidx[l+1]]
        position[lapidx[l]+1:lapidx[l+1]+1] += lapcount
        lapcount += 1
    if not lapidx[-1] == len(position):
        position[lapidx[-1]+1:] /= averagelap
        position[lapidx[-1]+1:] += lapcount
        lapcount += 1
    position *= beltLength
    
    posfunc = interp1d(timeticks, position,
                       kind='linear',
                       bounds_error=False,
                       fill_value=np.min(position),
                       assume_sorted=True)
    x = np.arange(0,np.max(timeticks),1./samplingRate)
    y = posfunc(x)
    return x,y

def treadmillBehaviorFromRelativePosition(time,pos,
                                          srate = 90.):
    ''' Interpolates relative position to account for lap drifts of the belt.
        Position and displacement and velocity need to be multiplied by the belt length.
    Outputs:
        behaviortime,position,displacement,velocity,laptimes
        '''

    lapidx = np.where(np.diff(pos) < -100)[0]

    if len(lapidx) < 1:
        # this is not correct but handles lack of behavior
        position = pos/pos.max()
        displacement = pos/pos.max()
        laptimes = []
    else:
        laptimes = time[lapidx]

        displacement = np.zeros_like(pos,dtype=np.float32)
        position = np.zeros_like(pos,dtype=np.float32)
        meanlapticks = int(mode(pos[lapidx]).mode)

        # Fix the first lap
        idx = np.arange(0,lapidx[0]+1)
        position[idx] = pos[idx]/pos[idx[-1]]
        displacement[idx] = pos[idx]/pos[idx[-1]]
        lapcounter = 1.
        for ii,lapstart in enumerate(lapidx[:-1]):
            idx = np.arange(lapstart+1, lapidx[ii+1]+1)
            position[idx] = pos[idx]/pos[idx[-1]]
            displacement[idx] = pos[idx]/pos[idx[-1]] + lapcounter
            lapcounter += 1
        # Fix the last lap
        idx = np.arange(lapidx[-1]+1,len(pos))
        position[idx] = pos[idx]/meanlapticks
        displacement[idx] = pos[idx]/meanlapticks + lapcounter
    # Interpolate to get a uniform sampling rate
    behaviortime = np.arange(np.min(time),np.max(time),1./srate)
    position = interp1d(time,position,
                        kind = 'nearest',
                        copy=False,
                        bounds_error=False,
                        fill_value=np.nan)(behaviortime)
    displacement = interp1d(time,displacement,
                            kind = 'linear',
                            copy=False,
                            bounds_error=False,
                            fill_value='extrapolate')(behaviortime)
    velocity = np.diff(np.hstack([displacement[0],displacement]))/(1./srate)
    return behaviortime,position,displacement,velocity,laptimes

defaultPreferences = dict(monitor = [dict(name = 'test',
                                          rate = 60.,
                                          distance = 18,
                                          sizePix = (1000,1000),
                                          gamma = 1.,
                                          width = 25,
                                          fullScreen = False,
                                          screen = 1
                                     )],
                          flashIndicator=True,
                          flashIndicatorMode = 0,
                          flashIndicatorParameters = dict(size = 100,
                                                          units = 'pix',
                                                          pos = [-300,300]),
                          warp =  dict(warp='spherical',
                                       warpfile = "",
                                       warpGridsize = 300,
                                       eyepoint = [0.5, 0.5],
                                       flipHorizontal = False,
                                       flipVertical = False),
                          defaultExperimentType = 'test',
                          logFolder = 'C:\\data\\vstim\\presentation',
                          stimsFolder = 'C:\\data\\vstim\\stims',
                          tmpFolder = None,
                          protocolsFolder = None,
                          userPrefix='JC')


def getPreferences(user='default',no_create=False):
    ''' Reads/creates the user preferences from the home directory.

    pref = getPreferences(user)

    Joao Couto - May 2017
    '''
    preferencepath = pjoin(os.path.expanduser('~'),'vstim',user)

    if not os.path.exists(preferencepath):
        if no_create:
            return None
        os.makedirs(preferencepath)
        print('Creating preferences folder ['+preferencepath+']')
    preffile = pjoin(preferencepath,'preferences.json')
    if not os.path.isfile(preffile):
        if defaultPreferences['tmpFolder'] is None:
            defaultPreferences['tmpFolder'] = preferencepath
        if defaultPreferences['protocolsFolder'] is None:
            defaultPreferences['protocolsFolder'] = pjoin(preferencepath,
                                                          'protocols')
            if not os.path.isdir(pjoin(preferencepath,
                                       'protocols')):
                os.makedirs(pjoin(preferencepath,
                                  'protocols'))
                #os.path.dirname(os.path.dirname(pv.__file__))

        with open(preffile, 'w') as outfile:
            json.dump(defaultPreferences,
                      outfile,
                      sort_keys = True,
                      indent = 4)
            print('Saving default preferences to: ' + preffile)
    with open(preffile, 'r') as infile:
        pref = json.load(infile)
    pref['user'] = user
    return pref


from tqdm import tqdm
class progBar(tqdm):
    def __init__(self):
        super(progBar,self).__init__()
    def setTotal(self,N):
        self.total = N
    def setValue(self, val):
        self.value = val
