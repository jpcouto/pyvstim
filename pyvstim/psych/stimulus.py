# /usr/bin/env python
#
# Classes to describe various stimulus.
#
from psychopy import visual
import sys
import os
import numpy as np
from scipy import misc
import pandas as pd
from tqdm import tqdm
from ..utils import * 

######################################################################################
# Base stimulus class
######################################################################################
class Stimulus(object):
    def __init__(self,experiment,**kwargs):
        ''' Abstract class to describe a stimulus.
        A stimulus must be allowed to evolve during each frame before the flip.'''
        self.exp = experiment
        return    
    def evolve(self):
        self.frame += 1
        print('Frame: ' + str(self.frame))
        code = 1
        # self.stim.draw() # Goes here...
        return code,[]

######################################################################################
# Full-field grating 
######################################################################################

class Gratings(Stimulus):
    frame = 0
    trial = 0
    stimName = 'gratings'

    logHEADER = 'code,presentTime,iStim,iTrial,iFrame,blank,contrast,orientation,tf,sf,phase,posx,posy,size'
    logMSG = lambda _stimulus,_trial,_frame: '{0},{1},{2}'.format(_stimulus,_trial,_frame)
    #exp = None    

    def __init__(self,experiment,iWindow = 0, ntrials = 1,
                 contrast = [0.8],
                 iStims = None,
                 ori = [0],
                 sf=[0.1],
                 tf = [2], 
                 tex = 'sqr',
                 dur = [2.],
                 size = None,
                 reps = 1,
                 x=[0],
                 y=[0],
                 shuffle  = False,
                 blankDur = 1.,
                 groupStimuliTrials = False,
                 mask = None,
                 aperture = None,
                 printStimParams = False,
                 background = 0,
                 interpolate = False,
                 **kwargs):
        ''' Class to describe a grating stimulus.
        '''
        super(Gratings,self).__init__(experiment)
        self.iWindow = iWindow
        self.nStims = len(contrast)
        self.stimuli = dict()
        self.durationFrames = None
        self.nReps = reps
        self.blankDuration = float(blankDur)
        
        if size is None:
            size = [[0,0] for t in range(self.nStims)]
        if iStims is None:
            iStims =  np.arange(self.nStims,dtype=int)
        for i,s in enumerate(size):
            if s[0] == 0:
                size[i][0] = 220#self.exp.size[0]
            if s[1] == 0:
                size[i][1] = 220#self.exp.size[1]
        # Update and check sizes
        for iStim in range(self.nStims):
            nFrames = int(dur[iStim]*self.exp.refreshRate)
            if not type(contrast[iStim]) == np.array:
                contrast[iStim] = np.ones((nFrames,))*contrast[iStim]
            if not type(ori[iStim]) == np.array:
                ori[iStim] = np.ones((nFrames,))*ori[iStim]
            if not type(tf[iStim]) == np.array:
                tf[iStim] = np.ones((nFrames,))*tf[iStim]
            if not type(sf[iStim]) == np.array:
                sf[iStim] = np.ones((nFrames,))*sf[iStim]
            if not type(x[iStim]) == np.array:
                x[iStim] = np.ones((nFrames,))*x[iStim]
            if not type(y[iStim]) == np.array:
                y[iStim] = np.ones((nFrames,))*y[iStim]

        if mask is None or type(mask) is str:
            mask = [mask for t in range(self.nStims)]    
        if not aperture is None:
            apx = []
            apy = []
            for ii in range(self.nStims):
                nFrames = int(dur[ii]*self.exp.refreshRate)

                if not type(aperture['x'][ii]) == np.array:
                    apx.append(np.ones((nFrames,))*aperture['x'][ii])
                else:
                    apx.append(aperture['x'][ii])
                if not type(aperture['y'][ii]) == np.array:
                    apy.append(np.ones((nFrames,))*aperture['y'][ii])
                else:
                    apy.append(aperture['y'][ii])
                    
        self.blankFrames = int(np.floor(self.exp.refreshRate)*self.blankDuration)

        # Load Stimuli to stim dictionary
        for ii,(iStim,c,o,t,s,d,xc,yc,sz) in enumerate(zip(iStims,contrast,
                                                           ori,
                                                           tf,
                                                           sf,
                                                           dur,
                                                           x,y,
                                                           size)):
            tmp = dict()
            if ii > 0 and iStim == iStims[ii-1]:
                nBlankFrames = 0
            else:
                nBlankFrames = self.blankFrames
                
            # aperture parameters
            tmp.update(dict(iStim=iStim,
                            duration = d,
                            durationFrames = d*self.exp.refreshRate,
                            contrast = c,
                            orientation = o,
                            tf = t,
                            sf = s,
                            posx = xc,
                            posy = yc,
                            size = sz,
                            nBlankFrames = nBlankFrames))
            if not aperture is None:
                tmp.update(dict(apSize = aperture['size'][ii],
                                apx = apx[ii],
                                apy = apy[ii]))
            tmp['mask'] = mask[ii]
            tmp['stim'] = visual.GratingStim(self.exp.window[self.iWindow],
                                             tex=tex,
                                             contrast=c[0],
                                             ori=o[0],
                                             sf=s[0],
                                             pos=[xc[0],yc[0]],
                                             size=s[0],
                                             mask=mask[ii],
                                             name=self.stimName,
                                             units=self.exp.units,
                                             interpolate=interpolate)
            self.stimuli[ii] = tmp.copy()      

        stimOrder = []
        if not groupStimuliTrials:
            for iTrial in range(self.nReps):
                nGroups = len(np.unique(iStims))
                nStims = len(iStims)
                nPerGroup = int(nStims/nGroups)
                idx2d = np.arange(nStims,dtype = int).reshape((nGroups,nPerGroup))
                idx = np.arange(nGroups,dtype = int)
                if shuffle:
                    idx = np.random.choice(idx,nGroups,False)
                idx = idx2d[idx,:].flatten()
                for iStim in idx:
                    stimOrder.append([iStim,iTrial])
        else:
            for iStim in range(self.nStims):
                for iTrial in range(self.nReps):
                    stimOrder.append([iStim,iTrial])
        self.stimOrder = np.vstack(stimOrder)
        self.logHeader = 'code,presentTime,iStim,iTrial,iFrame,blank,contrast,orientation,tf,sf,phase,posx,posy,size'
        if not aperture is None:
            self.logHeader += ',apx,apy'
        print('Blank frames:' + str(self.blankFrames))
        self.frameCnt = 0
        self.iFrame = 0
        self.expcnt = 0
        self.iStims = iStims
        # Warning: For now this takes the first mask. Need to make one Grating stim per stimtype to make it work.
        self.blank = visual.GratingStim(self.exp.window[self.iWindow],
                                        tex=None,
                                        contrast=background,
                                        ori=0,
                                        sf=0,
                                        pos=[0,0],
                                        size=300,
                                        mask=None,
                                        name='blank',
                                        units=self.exp.units,
                                        interpolate=False)
        durationFrames = np.array([di['durationFrames'] for di in self.stimuli.values()])
        nBlankFrames = np.array([di['nBlankFrames'] for di in self.stimuli.values()])
        self.nFramesTotal = np.sum(durationFrames)*self.nReps + np.sum(nBlankFrames)*self.nReps + self.blankFrames
        self.exp.setnFramesTotal(self.nFramesTotal)

        if not aperture is None:
            self.aperture = visual.Aperture(self.exp.window[self.iWindow],
                                            size=[300,300],
                                            shape=aperture['mask'])
            self.aperture.disable()
        else:
            self.aperture = None
        # In the future will become self.exp.window[iWindow] (multi screen)
        self.exp.setBackground(background)
        self.stimcnt = 0
        return

    def evolve(self):
        iPointer = np.clip(self.expcnt,0,self.stimOrder.shape[0]-1)
        iStim,iTrial = self.stimOrder[iPointer]
        blank = 1
        iFrame = 0
        code = 0      

        # Are we done with the experiment? Present blank frames
        if self.expcnt >= self.stimOrder.shape[0]:
            if self.iFrame < self.blankFrames:
                toLog = [ -1,
                          -1,
                          iFrame,
                          blank,
                          0,
                          0,
                          0,
                          0,
                          0,
                          0,
                          0]
                self.blank.draw()
                self.iFrame += 1
                return 0,toLog
            else:
                return -1,[]
        if self.stimcnt != self.expcnt:
            # to update the indicator only at start
            self.stimcnt=self.expcnt
        # Define stimulus parameters.
        iFrame = int(np.mod(self.iFrame-self.stimuli[iStim]['nBlankFrames'],self.stimuli[iStim]['durationFrames']))
        if self.iFrame <= self.stimuli[iStim]['nBlankFrames']:
            stim = self.blank
            blank = 1
            code = 0
            if self.iFrame == self.stimuli[iStim]['nBlankFrames']:
                self.phaseAdvance = self.stimuli[iStim]['tf'][iFrame]/float(self.exp.refreshRate)
                self.stimuli[iStim]['stim'].phase = -1*self.phaseAdvance
        elif self.iFrame >= self.stimuli[iStim]['nBlankFrames']+self.stimuli[iStim]['durationFrames']:
            self.iFrame = 0
            self.expcnt += 1
        if self.iFrame >= self.stimuli[iStim]['nBlankFrames']:
            code = 1
            blank = 0
            stim = self.stimuli[iStim]['stim']
            self.phaseAdvance = self.stimuli[iStim]['tf'][iFrame]/float(self.exp.refreshRate)
            stim.phase += self.phaseAdvance
            stim.contrast = self.stimuli[iStim]['contrast'][iFrame]
            stim.ori = self.stimuli[iStim]['orientation'][iFrame]
            stim.sf = self.stimuli[iStim]['sf'][iFrame]
            stim.setSize(self.stimuli[iStim]['size'])
            stim.pos = [self.stimuli[iStim]['posx'][iFrame],
                        self.stimuli[iStim]['posy'][iFrame]]
            if not self.aperture is None:
                self.aperture.setPos([self.stimuli[iStim]['apx'][iFrame],
                                      self.stimuli[iStim]['apy'][iFrame]])
                self.aperture.setSize(self.stimuli[iStim]['apSize'])
                self.aperture.enable()
        toLog = [ iStim,
                  iTrial + 1,
                  iFrame,
                  blank,
                  stim.contrast,
                  stim.ori,
                  self.stimuli[iStim]['tf'][iFrame],
                  stim.sf,
                  stim.phase[0],
                  self.stimuli[iStim]['posx'][iFrame],
                  self.stimuli[iStim]['posy'][iFrame]]
        stim.draw()

        
        if not self.aperture is None:
            self.aperture.disable()
            toLog += [self.stimuli[iStim]['apx'][iFrame],
                      self.stimuli[iStim]['apx'][iFrame]]
        self.iFrame += 1
        self.frameCnt += 1
        # Advance stim if done with it.
        if self.iFrame > self.stimuli[iStim]['nBlankFrames']:
            if iFrame == self.stimuli[iStim]['durationFrames']-1:
                self.iFrame = 0
                self.expcnt += 1
        self.iStim = iStim
        return code,toLog
    
######################################################################################
# Stimulus for airpuff during stimulus presentation 
######################################################################################
class ActuatorGratings(Gratings):
    def __init__(self,experiment,iWindow = 0,ntrials = 1,
                 contrast = 0.8,
                 iStims = None,
                 ori = [0],sf=[0.1],tf = [2], 
                 tex = 'sqr',#sin
                 dur = [2.],
                 size = None,
                 reps = 1,
                 x=0,
                 y=0,
                 shuffle  = False,
                 blankDur = 1,
                 groupStimuliTrials = False,
                 mask = None,
                 aperture = None,
                 printStimParams = False,
                 background = 0,
                 interpolate = False,
                 frequency = 0.1,
                 actuator = [1],
                 actuatorWidth = 0.05,
                 **kwargs):
        ''' Class to describe a grating stimulus.
        '''
        super(ActuatorGratings,self).__init__(experiment = experiment,
                                              iWindow= iWindow,
                                              contrast = contrast,
                                              iStims = iStims,
                                              ori = ori,
                                              sf = sf,
                                              tf = tf, 
                                              tex = tex,#sin
                                              dur = dur,
                                              size = size,
                                              reps = reps,
                                              x = x,
                                              y = y,
                                              shuffle = shuffle,
                                              blankDur = blankDur,
                                              groupStimuliTrials = groupStimuliTrials,
                                              mask = mask,
                                              aperture = aperture,
                                              printStimParams = printStimParams,
                                              background = background,
                                              interpolate = interpolate)

        self.frequency = frequency
        self.frequencyFrames = (1./frequency)*self.exp.refreshRate
        self.actuator = actuator
        self.lastiStim = -1
        if not type(actuatorWidth) in [np.ndarray, list]:
            actuatorWidth = np.array([actuatorWidth]*len(self.frequency))
        self.pulseWidth = actuatorWidth
        self.period = 1./frequency - self.pulseWidth
    def evolve(self):
        code,toLog = super(ActuatorGratings,self).evolve()
        
        i = self.iStim #np.where(self.iStims == self.iStim)[0]
        frequencyFrames = np.unique(self.frequencyFrames[i])
        if not frequencyFrames  == np.inf:
            if not self.lastiStim == self.iStim:
                self.lastiStim = self.iStim
                self.exp.rig.setActuatorParameters(
                    actuator = self.actuator[i],
                    npulses =int(
                        np.ceil((self.stimuli[self.iStim]['durationFrames']/self.exp.refreshRate)*self.frequency[i])),
                    width = int(self.pulseWidth[i]*1000),
                    period = int(self.period[i]*1000),
                    position_trigger = -1)
            if self.iFrame == self.blankFrames:
                if self.actuator[i] == 1:
                    self.exp.rig.activateActuator1()
                else:
                    self.exp.rig.activateActuator0()
                print('Actuator')
        return code,toLog
    
######################################################################################
# Stimulus for displaying sequences of images 
######################################################################################
class ImageSequence(Stimulus):
    frame = 0
    trial = 0
    stimName = 'ImageSequence'
    exp = None

    def __init__(self,experiment,iWindow = 0,stimFrames = None,stimFramePaths = None,
                 ntrials = 1,
                 contrast = 0.8,
                 dur = [2.],
                 ori = None,
                 size = None,
                 reps = 1,
                 x=0,
                 y=0,
                 shuffle  = False,
                 blankDur = 1,
                 decimationFactor = 2,
                 groupStimuliTrials = False,
                 mask = None,
                 aperture = None,
                 printStimParams=False,
                 interpolate = True,
                 progress = None,
                 loopBack = False,
                 background = 0,
                 **kwargs):
        ''' Abstract class to describe a stimulus.
        A stimulus must be allowed to evolve during each frame before the flip.'''
        super(ImageSequence,self).__init__(experiment)
        self.iWindow = iWindow
        self.durationFrames = None
        self.decimationFactor = decimationFactor
        self.nReps = reps
        self.nStims = len(contrast)
        self.loopBack = loopBack
        self.blankDuration = blankDur
        if size is None:
            size = [[0,0] for t in contrast]
        if ori is None:
            ori = [0.0 for t in contrast]
        for i,s in enumerate(size):
            try:
                len(s) < 2
            except:
                s = [s]*2
            if s[0] == 0:
                size[i][0] = 300
            if s[1] == 0:
                size[i][1] = 300
        self.iStims = np.arange(len(contrast),dtype=int)
        params = []
        if mask is None or type(mask) is str:
            mask = [mask for t in contrast]
        if not groupStimuliTrials:
            for iTrial in range(self.nReps):
                p = []
                for iStim,(c,d,o,xc,yc,sz) in enumerate(zip(contrast,dur,ori,x,y,size)):
                    nFrames = int(np.floor(d*self.exp.refreshRate))
                    if not type(xc) == np.array:
                        xc = np.ones((nFrames,))*xc
                    if not type(yc) == np.array:
                        yc = np.ones((nFrames,))*yc
                    stimdesc = [iStim,iTrial,d,nFrames,c,o,xc,yc,sz]
                    # aperture parameters
                    if not aperture is None:
                        if not type(aperture['x'][iStim]) == np.array:
                            apx = np.ones((nFrames,))*aperture['x'][iStim]
                        if not type(aperture['y'][iStim]) == np.array:
                            apy = np.ones((nFrames,))*aperture['y'][iStim]
                        stimdesc += [aperture['size'][iStim],apx,apy]
                    p.append(stimdesc)
                if shuffle:
                    idx = np.random.choice(np.arange(len(p)),len(p),False)
                    print('Block order: [{0}]: {1}'.format(iTrial,','.join([str(i) for i in idx])))
                else:
                    idx = np.arange(len(p))
                
                for i in idx:
                    params.append(p[i])
        else:
            # For looping trials by stimuli
            for iStim,(c,d,o,xc,yc,sz) in enumerate(zip(contrast,dur,ori,x,y,size)):
                for iTrial in range(self.nReps):
                    nFrames = int(np.floor(d*self.exp.refreshRate))
                    if not type(xc) == np.ndarray:
                        xc = np.ones((nFrames,))*xc
                    if not type(yc) == np.ndarray:
                        yc = np.ones((nFrames,))*yc
                    if not len(xc) == nFrames:
                        xc = np.ones((nFrames,))*xc
                    if not len(yc) == nFrames:
                        yc = np.ones((nFrames,))*yc
                    stimdesc = [iStim,iTrial,d,nFrames,c,o,xc,yc,sz]
                    # aperture parameters
                    if not aperture is None:
                        if not type(aperture['x'][iStim]) == np.ndarray:
                            apx = np.ones((nFrames,))*aperture['x'][iStim]
                        else:
                            apx = aperture['x'][iStim]
                        if not type(aperture['y'][iStim]) == np.ndarray:
                            apy = np.ones((nFrames,))*aperture['y'][iStim]
                        else:
                            apy = aperture['y'][iStim]
                        stimdesc += [aperture['size'][iStim],apx,apy]
                    params.append(stimdesc)
        paramkeys = ['iStim',
                     'iTrial',
                     'duration',
                     'durationFrames',
                     'contrast',
                     'orientation',
                     'posx',
                     'posy',
                     'size']
        self.logHeader = 'code,presentTime,iStim,iTrial,iFrame,blank,contrast,posx,posy'
        if not aperture is None:
            paramkeys += ['apSize','apx','apy']
            self.logHeader += ',apx,apy' 
        self.params = pd.DataFrame(params,columns=paramkeys)
        if printStimParams:
            print(self.params)
        self.blankFrames = int(np.floor(self.exp.refreshRate)*blankDur)
        self.frameCnt = 0
        self.iFrame = 0
        self.expcnt = 0
        self.blank = visual.GratingStim(self.exp.window[self.iWindow],
                                        tex=None,
                                        contrast=background,
                                        ori=0,
                                        sf=0,
                                        pos=[0,0],
                                        size=300,
                                        mask=None,
                                        name='blank',
                                        units=self.exp.units,
                                        interpolate=False)
        if stimFrames is None:
            stims = stimFramePaths
        else:
            stims = stimFrames
        self.stimuli = []
        if progress is None:
            pbar = tqdm(desc = 'Loading stimuli')
            pbar.total = np.sum([len(s) for s in stims])
        else:
            pbar = progress
            pbar.setTotal(np.sum([len(s) for s in stims]))
        for iStim,frames in enumerate(stims):
            self.stimuli.append([])
            for iFrame,frame in enumerate(frames):
                #if type(frame) is str or unicode:
                #    print(frame)
                #    try:
                #        image = (misc.imread(frame)[:,:,0].astype('float32')-128)/128.
                #    except IndexError:
                #        image = (misc.imread(frame)[:,:].astype('float32')-128)/128.
                #else:
                #    image = frame
                self.stimuli[-1].append(visual.ImageStim(self.exp.window[self.iWindow],
                                                         image=frame,
                                                         contrast=self.params.iloc[iStim].contrast,
                                                         mask=mask[iStim],
                                                         name='Stim{0:03d}_Frame{0:04d}'.format(iStim,iFrame),
                                                         pos=[0,0],
                                                         ori = self.params.iloc[iStim].orientation,
                                                         size=size[iStim],
                                                         interpolate=interpolate))
                pbar.update(1)
        try:
            pbar.close()
        except:
            pass
        self.nFramesTotal = self.params.durationFrames.sum() + self.blankFrames*(len(self.params)+1)
        self.exp.setnFramesTotal(self.nFramesTotal)

        if not aperture is None:
            self.aperture = visual.Aperture(self.exp.window[self.iWindow],
                                            size=[300,300],
                                            shape=aperture['mask'])
            self.aperture.disable()
        else:
            self.aperture = None
        self.exp.setBackground(background)
        self.loopBackFactor = -1
        self.stimcnt = 0
        return

    def evolve(self):

        i = np.mod(self.expcnt,len(self.params))
        iFrame = self.iFrame-self.blankFrames
        iFrameStim = -1
        blank = 1
        code = 0
        if self.expcnt >= len(self.params) and not self.iFrame < self.blankFrames:
            self.exp.stop()
            return -1,[]
        elif self.iFrame < self.blankFrames:
            stim = self.blank
            iFrame = 0
        elif self.iFrame >= self.blankFrames:
            blank = 0
            code = 1
            iStimFrame = self.params.iloc[i]['iStim']
            iFrameStim = np.mod(int(np.floor(iFrame/float(self.decimationFactor))),
                                len(self.stimuli[iStimFrame]))
            if self.loopBack:
                tmpnstims = len(self.stimuli[iStimFrame])
                if int(np.floor(iFrame/float(self.decimationFactor))) > tmpnstims-1:
                    if np.mod(int(np.floor(iFrame/float(self.decimationFactor)))/tmpnstims*2,2) == 0:
                        self.loopBackFactor = -1
                    else:
                        self.loopBackFactor = 1
                    # This is still not correct
                    if self.loopBackFactor == -1:
                        iFrameStim = self.loopBackFactor * (iFrameStim + 1)
            stim = self.stimuli[iStimFrame][iFrameStim]
            stim.contrast = self.params.iloc[i]['contrast']
            stim.setSize(self.params.iloc[i]['size'])
            stim.pos = [self.params.iloc[i]['posx'][iFrame],
                        self.params.iloc[i]['posy'][iFrame]]
            if not self.aperture is None:
                self.aperture.setPos([self.params.iloc[i]['apx'][iFrame],
                                      self.params.iloc[i]['apy'][iFrame]])
                self.aperture.setSize(self.params.iloc[i]['apSize'])
                self.aperture.enable()
        if self.stimcnt != self.expcnt:
            # to update the indicator only at start
            self.stimcnt = self.expcnt
        toLog = [ self.params.iloc[i]['iStim']+1,
                  self.params.iloc[i]['iTrial']+1,
                  iFrameStim,
                  blank,
                  self.params.iloc[i]['contrast'],
                  self.params.iloc[i]['posx'][iFrame],
                  self.params.iloc[i]['posy'][iFrame]]
        stim.draw()
        if not self.aperture is None:
            self.aperture.disable()
            toLog += [self.params.iloc[i]['apx'][iFrame],
                      self.params.iloc[i]['apy'][iFrame]]
        self.iFrame += 1
        self.frameCnt += 1
        self.iStim = self.params.iloc[i]['iStim']
        if self.iFrame >= self.blankFrames:
            if iFrame == self.params.iloc[i].durationFrames-1:
                self.iFrame = 0
                self.expcnt += 1
        return code,toLog #[t for t in np.array(toLog)]

class ActuatorImageSequence(ImageSequence):
    def __init__(self,experiment,iWindow = 0,
                 stimFrames = None,
                 stimFramePaths = None,
                 ntrials = 1,
                 contrast = 0.8,
                 dur = [2.],
                 size = None,
                 reps = 1,
                 x=0,
                 y=0,
                 shuffle  = False,
                 blankDur = 1,
                 decimationFactor = 2,
                 groupStimuliTrials = False,
                 mask = None,
                 aperture = None,
                 printStimParams=False,
                 interpolate = True,
                 progress = None,
                 loopBack = False,
                 background = 0,
                 frequency = 0.1,
                 actuator = 0,
                 actuatorWidth = 0.004,
                 **kwargs):
        ''' Class to describe a grating stimulus.
        '''
        super(ActuatorImageSequence,self).__init__(experiment = experiment,
                                                   iWindow=iWindow,
                                                   stimFrames = stimFrames,
                                                   stimFramePaths = stimFramePaths,
                                                   ntrials = ntrials,
                                                   contrast = contrast,
                                                   dur = dur,
                                                   size = size,
                                                   reps = reps,
                                                   x=x,
                                                   y=y,
                                                   shuffle  = shuffle,
                                                   blankDur = blankDur,
                                                   decimationFactor = decimationFactor,
                                                   groupStimuliTrials = groupStimuliTrials,
                                                   mask = mask,
                                                   aperture = aperture,
                                                   printStimParams=printStimParams,
                                                   interpolate = interpolate,
                                                   progress = progress,
                                                   loopBack = loopBack,
                                                   background = background)
        self.frequency = frequency
        self.frequencyFrames = (1./frequency)*self.exp.refreshRate
        self.actuator = actuator
        self.lastiStim = -1
        if not type(actuatorWidth) in [np.ndarray, list]:
            actuatorWidth = np.array([actuatorWidth]*len(self.frequency))
        self.pulseWidth = actuatorWidth
        self.period = 1./frequency - self.pulseWidth

    def evolve(self):
        code,toLog = super(ActuatorImageSequence,self).evolve()
        i = np.where(self.iStims == self.iStim)[0]
        frequencyFrames = np.unique(self.frequencyFrames[i])
        if not frequencyFrames  == np.inf:
            if not self.lastiStim == self.iStim:
                self.lastiStim = self.iStim
                self.exp.rig.setActuatorParameters(actuator = self.actuator[i][0],
                                                   npulses = int(
                                                       np.ceil(self.params.iloc[np.mod(self.expcnt,len(self.params))].duration*self.frequency[i])),
                                                   width = int(self.pulseWidth[i]*1000),
                                                   period = int(self.period[i]*1000))
            if self.iFrame == self.blankFrames:
                if self.actuator[i] == 1:
                    self.exp.rig.activateActuator1()
                else:
                    self.exp.rig.activateActuator0()
                print('Actuator')
        #frequencyFrames = self.frequencyFrames[self.iStims == self.iStim]
        #if not frequencyFrames  == np.inf:
        #    if np.mod(self.iFrame-self.blankFrames,frequencyFrames) == 0 and code>0:
        #        if self.actuator[self.iStims == self.iStim] == 1:
        #            self.exp.rig.setActuator1()
        #        else:
        #            self.exp.rig.setActuator0()
        return code,toLog

class GatedPMTStim(Gratings,ImageSequence):
    def __init__(self,experiment,
                 stimType = 'gratings',
                 stimflag = None,
                 npulses = 10,
                 width = 10,
                 margin = 2,
                 **kwargs):
        ''' Class to describe the shutter control stimulus.
        WARNING: Do not use. until npulses are set to 1
        '''
        if stimType.lower() == 'gratings':
            self.stimHandler = Gratings(experiment = experiment,
                                     **kwargs)
        else:
            self.stimHandler = ImageSequence(experiment = experiment,
                                          **kwargs)
        self.stimcnt = self.stimHandler.stimcnt
        self.logHeader = self.stimHandler.logHeader
        self.exp = experiment
        self.stimflag = stimflag
        self.exp.PMTgate.set_parameters(margin = margin,width = width,npulses = npulses)

    def evolve(self):
        code,toLog = self.stimHandler.evolve()
        if self.stimHandler.iFrame == int(self.stimHandler.blankFrames/2):
            sIdx = np.where(self.stimHandler.iStims == self.stimHandler.iStim)[0]
            print('Stimuli {0} armed PMT stim'.format(sIdx))

            if self.stimflag[sIdx]:
                self.exp.PMTgate.arm()
        return code,toLog


class ServoStageStim(Stimulus):
    def __init__(self,experiment,
                 iWindow=0,
                 reps = 1,
                 duration=None,
                 stage_position = None,
                 shuffle = False,
                 background = None,
                 blankDur=None,
                 stimType = 'servo_stage',
                 stimflag = None,
                 printStimParams=False,

                 **kwargs):
        ''' Servo stage control stimulus for whisker stimulation.
        
        '''
        super(ServoStageStim,self).__init__(experiment)
        
        if not hasattr(self.exp,'ServoStage'):
            print('Need to initialize the ServoStage first.')
            return
        self.controller = self.exp.ServoStage
        self.iWindow = iWindow
        self.durationFrames = None
        self.nReps = reps
        self.nStims = len(stage_position)
        self.blankDuration = blankDur
        self.iStims = np.arange(len(stage_position),dtype=int)
        params = []
        for iTrial in range(self.nReps):
            p = []
            for iStim,(d,c) in enumerate(zip(duration,stage_position)):
                nFrames = int(np.floor(d*self.exp.refreshRate))
                stimdesc = [iStim,iTrial,d,c,nFrames]
                p.append(stimdesc)
            if shuffle:
                idx = np.random.choice(np.arange(len(p)),len(p),False)
                print('Block order: [{0}]: {1}'.format(iTrial,','.join([str(i) for i in idx])))
            else:
                idx = np.arange(len(p))
                
            for i in idx:
                params.append(p[i])
        paramkeys = ['iStim',
                     'iTrial',
                     'duration',
                     'stagePosition',
                     'durationFrames']
        self.logHeader = 'code,presentTime,iStim,iTrial,iFrame,blank,stagePosition'
        self.params = pd.DataFrame(params,columns=paramkeys)
        if printStimParams:
            print(self.params)
        self.blankFrames = int(np.floor(self.exp.refreshRate)*blankDur)
        self.frameCnt = 0
        self.iFrame = 0
        self.expcnt = 0
        self.blank = visual.GratingStim(self.exp.window[self.iWindow],
                                        tex=None,
                                        contrast=background,
                                        ori=0,
                                        sf=0,
                                        pos=[0,0],
                                        size=300,
                                        mask=None,
                                        name='blank',
                                        units=self.exp.units,
                                        interpolate=False)
        self.nFramesTotal = self.params.durationFrames.sum() + self.blankFrames*(len(self.params)+1)
        self.exp.setnFramesTotal(self.nFramesTotal)
        self.exp.setBackground(background)
        self.stimcnt = 0
        return

    def evolve(self):
        i = np.mod(self.expcnt,len(self.params))
        iFrame = self.iFrame-self.blankFrames
        iFrameStim = -1
        blank = 1
        code = 0
        if self.expcnt >= len(self.params) and not self.iFrame < self.blankFrames:
            self.exp.stop()
            return -1,[]
        elif self.iFrame < self.blankFrames:
            stim = self.blank
            iFrame = 0
        elif self.iFrame >= self.blankFrames:
            blank = 0
            code = 1
            stim = self.blank
        if self.stimcnt != self.expcnt:
            # to update the indicator only at start
            self.stimcnt = self.expcnt
        toLog = [ self.params.iloc[i]['iStim']+1,
                  self.params.iloc[i]['iTrial']+1,
                  iFrameStim,
                  blank,
                  self.params.iloc[i]['stagePosition']]
        stim.draw()
        self.iFrame += 1
        self.frameCnt += 1
        self.iStim = self.params.iloc[i]['iStim']
        if self.iFrame >= self.blankFrames:
            if iFrame == self.params.iloc[i].durationFrames-1:
                self.iFrame = 0
                self.expcnt += 1
        if self.iFrame == int(self.blankFrames/2):
            print('Stimuli {0} moving stage to {1} and arming servo.'.format(self.params.iloc[i]['iStim'],
                                                                             self.params.iloc[i]['stagePosition']))
            self.controller.servo_set_duration(self.params.iloc[i]['duration'])
            self.controller.arm(position=self.params.iloc[i]['stagePosition'])
        return code,toLog


    
######################################################################################
# Shutter control between stimuli presentation
######################################################################################
class Shutter(Gratings,ImageSequence):
    def __init__(self,experiment,
                 stimType = 'gratings',
                 actuator = None,
                 shutter = None,
                 **kwargs):
        ''' Class to describe the shutter control stimulus.
        WARNING: Do not use. until npulses are set to 1
        '''
        if stimType.lower() == 'gratings':
            self.stimHandler = Gratings(experiment = experiment,
                                     **kwargs)
        else:
            self.stimHandler = ImageSequence(experiment = experiment,
                                          **kwargs)
        self.exp = experiment
        self.shutter = shutter
        self.actuator = actuator
        self.shutterFlag = False
        self.logHeader = self.stimHandler.logHeader
        self.exp.rig.setActuatorParameters(actuator = self.actuator,
                                           npulses = 1,
                                           width = int(10000),
                                           period = 1,
                                           position_trigger = -1)

    def evolve(self):
        code,toLog = self.stimHandler.evolve()
        if self.stimHandler.iFrame == int(self.stimHandler.blankFrames/2):
            sIdx = np.where(self.stimHandler.iStims == self.stimHandler.iStim)[0]
            print('Stimuli {0} shutter {1} was {2}'.format(sIdx,self.shutter[sIdx],int(self.shutterFlag)))

            if not int(self.shutter[sIdx]) == int(self.shutterFlag):
                self.shutterFlag = not self.shutterFlag
                if self.actuator[sIdx] == 1:
                    self.exp.rig.activateActuator1()
                else:
                    self.exp.rig.activateActuator0()
        return code,toLog

    
class ClosedLoopActuator(Stimulus):
    frame = 0
    trial = 0
    stimName = 'gratings'
    logHEADER = 'iStim,iTrial,actuator'
    exp = None

    def __init__(self,experiment,iWindow = 0,
                 dur = 20.*60,
                 actuatorPos = [3200],
                 actuator = [1],
                 actuatorFrequency = [20.],
                 actuatorDuration = [1.],
                 actuatorWidth = [0.01],
                 background = 0.,
                 blackLapProbability = 0.3,
                 **kwargs):
        ''' Class to describe a grating stimulus.
        '''
        super(ClosedLoopActuator,self).__init__(experiment)
        self.iWindow = iWindow
        self.durationFrames = None
        # create parameter space:
        params = []
        self.actuatorPos = actuatorPos

        self.blankFrames = int(np.floor(self.exp.refreshRate)*dur)
        nFrames = int(np.floor(dur*self.exp.refreshRate))
        self.logHeader = 'code,presentTime,iStim,contrast,actuator'
        print('Duration frames:' + str(self.blankFrames))
        self.frameCnt = 0
        self.iFrame = 0
        self.nFramesTotal = self.blankFrames
        self.exp.setnFramesTotal(self.nFramesTotal)
        self.background = background
        self.exp.setBackground(background)
        self.acted = [True for f in self.actuatorPos]
        self.actedFrame = [0 for f in self.actuatorPos]
        self.actFrame = 0
        self.lapnum = 0
        self.isDarkScreen = False
        self.blackLapProbability = blackLapProbability
        self.lastLap = -1
        self.frequency = np.array(actuatorFrequency)
        self.frequencyFrames = (1./self.frequency)*self.exp.refreshRate
        self.actuator = np.array(actuator)
        self.lastiStim = -1
        self.pulseWidth = np.array(actuatorWidth)
        self.actuatorDuration = np.array([float(i) for i in actuatorDuration])
        self.period = 1./self.frequency - self.pulseWidth
        print(self.actuator,self.pulseWidth,self.period,self.actuatorDuration)
        if len(self.actuator) > 1:
            print("Triggering at multiple positions is not supported (and only actuator zero in this version).")
        for i in range(len(self.actuator)):
            self.exp.rig.setActuatorParameters(actuator = self.actuator[i],
                                               npulses = int(
                                                   np.ceil(self.actuatorDuration[i]*self.frequency[i])),
                                               width = int(self.pulseWidth[i]*1000),
                                               period = int(self.period[i]*1000),
                                               position_trigger = self.actuatorPos[i])
        self.stimcnt = 0

    def evolve(self):
        # Are we done with the experiment? Present blank frames
        code = 0
        self.iFrame += 1
        if self.iFrame >= self.nFramesTotal:
            self.exp.stop()
            return -1,[]
        contrast = self.background
        if self.lastLap < self.exp.rigstatus['laps'][1]:
            self.acted = [False for i in self.acted]
            self.lastLap = self.exp.rigstatus['laps'][1]
            self.isDarkScreen = np.random.choice([False,True],
                                                 p=[1 -self.blackLapProbability,
                                                    self.blackLapProbability])
            if not self.isDarkScreen:
                self.exp.setBackground(self.background)
            else:
                self.exp.setBackground(-1)
                contrast = -1
        pos = self.exp.rigstatus['position'][1]
        actuator = -1
        for istim,p in enumerate(self.actuatorPos):
            if (pos >= p and not self.acted[istim]):
                self.acted[istim] = True
                self.actedFrame[istim] = self.iFrame
                #print("Setting acted on actuator {0}".format(self.actuator[istim]))
                if self.actuator[istim] == 1:
                    #self.exp.rig.activateActuator1()
                    actuator = 1
                else:
                    #self.exp.rig.activateActuator0()
                    actuator = 0
                    #print("Lap: {0} - position {1} actuator {2}".format(
                    #    self.lastLap + 1,
                    #    pos,
                    #    self.actuator[istim]))

                code = 1
        self.stimcnt = self.exp.rigstatus['laps'] 
        toLog = [self.stimcnt,
                 contrast,
                 code,actuator]
        return code,toLog
    
######################################################################################
# User controlled gratings stimulus class
######################################################################################

class UserControlledGrating(Stimulus):
    frame = 0
    trial = 0
    stimName = 'UserControlledGrating'
    logHeader = 'X,Y,OPACITY,CONTRAST,TF,SF,ORI'
    logMSG = lambda tilde,_x,_y,_opacity,_contrast,_tf,_sf,_ori: '''
{0},{1},{2},{3},{4},{5},{6}'''.format(_x,
                                      _y,
                                      _opacity,
                                      _contrast,
                                      _tf,
                                      _sf,
                                      _ori)
    exp = None
    size = 10
    def __init__(self,experiment,iWindow = 0,ntrials = 1,
                 contrast = 1.0,
                 ori = 0,
                 sf=0.1,
                 tf = 2, 
                 tex = 'sin',
                 dur = 1.,
                 size = 10,
                 mask = 'circle',
                 **kwargs):
        ''' Abstract class to describe a stimulus.
        A stimulus must be allowed to evolve during each frame before the flip.'''
        super(UserControlledGrating,self).__init__(experiment)
        self.iWindow = iWindow
        self.contrast = contrast
        self.ori = ori
        self.tf = tf
        self.sf = sf
        self.size = size
        # create parameter space [contrast,ori,sf,tf]:
        self.framespertrial = np.floor(self.exp.refreshRate)*dur
        self.framecnt = self.framespertrial + 1
        self.expcnt = -1
        self.stim = visual.GratingStim(self.exp.window[self.iWindow],tex=tex,
                                       contrast=self.contrast,
                                       ori=self.ori,
                                       sf=self.sf,
                                       mask=mask,
                                       name=self.stimName,
                                       pos=[0,0],
                                       size=self.size,
                                       units=self.exp.units)
        self.exp.setnFramesTotal(1000)

        from psychopy import event
        self.mouse = event.Mouse(visible=False)
        self.blinking = True
        self.blinkcount = 0
        self.blinkrate = 1.*self.exp.refreshRate
        barmask = -1*np.ones([64,64],dtype=np.float32)
        barmask[:,25:-25] = 1
        self.masks = ['circle',None,barmask,'gauss','raisedCos','cross']
        self.imask = 0
        self.texs = ['sin','sqr', 'saw', 'tri']
        self.itex = 1
        keyboard = {'l':lambda : self.changeTF(0.2),
                    'k':lambda : self.changeTF(-0.2),
                    'm':lambda : self.changeSF(0.01),
                    'n':lambda : self.changeSF(-0.01),
                    'v':lambda : self.changeORI(20),
                    'b':lambda : self.changeORI(-20),
                    'c':lambda : self.changeSIZE(2),
                    'x':lambda : self.changeSIZE(-2),
                    'z':lambda : self.toggleBlink(1),
                    's':lambda : self.changeMask(),
                    'd':lambda : self.changeTex(),
                    'f':lambda : self.printParameters()}
        print(''' 
    User controlled gratings: 
        l : increase TF
        k : decrease TF
        m : increase SF
        n : decrease SF
        v : increase orientarion
        b : decrese orientation
        c : increase size
        x : decrease size
        z : toggle blink (1 Hz)
        s : change mask
        d : change texture
        f : print parameters

    Warning: logging is not set up for this stimulus.
    ''')
        self.exp.setKeyActions(keyboard)
        self.stimcnt = 0
        return
    def printParameters(self):
        sys.stdout.write(''' 
    User controlled gratings parameters:
    temporal freq = {0}
    spatial freq  = {1}
    orientation   = {2}
    size          = {3}
    blink         = {4}
    position      = {5},{6}
    mask          = {7}
    texture       = {8}
'''.format(self.tf,
           self.sf,
           self.ori,
           self.size,
           self.blinking,
           self.stim.pos[0],self.stim.pos[1],
           self.imask,
           self.itex))
        sys.stdout.flush()
    def changeMask(self):
        self.imask += 1
        self.stim.setMask(self.masks[int(np.mod(self.imask,
                                                len(self.masks)))])
    def changeTex(self):
        self.itex += 1
        self.stim.tex = self.texs[int(np.mod(self.itex,
                                                len(self.texs)))]
    def changeTF(self,val):
        self.tf += val
    def changeSF(self,val):
        self.sf += val
    def changeORI(self,val):
        self.ori += val
    def changeSIZE(self,val):
        self.size += val
    def toggleBlink(self,blinkrate = 1.):
        self.blinking = not self.blinking
        self.blinkrate = blinkrate * self.exp.refreshRate
    def evolve(self):
        if self.blinking and np.mod(self.blinkcount,2) == 0:
            self.stim.opacity = 0
        else:
            self.stim.opacity = 1
        if np.mod(self.framecnt,self.blinkrate) == 0:
            self.blinkcount += 1
        self.stim.contrast = self.contrast
        self.stim.ori = self.ori
        self.stim.sf = self.sf
        self.stim.size = self.size
        self.phase = 0
        self.stim.phase += float(self.tf)/float(self.exp.refreshRate)
        self.stim.pos = self.mouse.getPos()
        code = 1
        self.framecnt += 1
        if self.stimcnt != self.expcnt:
            # to update the indicator only at start
            self.stimcnt=self.expcnt

        self.stim.draw()
        if np.mod(self.framecnt,120) == 0:
            self.expcnt += 1

        return code,[]
    def _MSG(self):
        return self.logMSG(self.stim.pos[0],self.stim.pos[1],
                           self.stim.opacity,self.stim.contrast,
                           self.tf,self.sf,self.stim.ori)











######################################################################################
# Stimulus for turning wheel behavioural setup 
######################################################################################
class WheelTask(ImageSequence):
    frame = 0
    trial = 0
    stimName = 'WheelTask'
    exp = None

    def __init__(self,experiment,iWindow = 0,stimFrames = None,stimFramePaths = None,
                 ntrials = 1,
                 contrast = 0.8,
                 dur = [2.],
                 ori = None,
                 size = None,
                 reps = 1,
                 x=0,
                 y=0,
                 shuffle  = False,
                 blankDur = 1,
                 decimationFactor = 2,
                 groupStimuliTrials = False,
                 mask = None,
                 aperture = None,
                 printStimParams=False,
                 interpolate = True,
                 progress = None,
                 loopBack = False,
                 background = 0,
                 rewardSize = 25,
                 **kwargs):
        ''' Abstract class to describe a stimulus.
        A stimulus must be allowed to evolve during each frame before the flip.'''
        super(WheelTask,self).__init__(experiment = experiment,
                                                   iWindow=iWindow,
                                                   stimFrames = stimFrames,
                                                   stimFramePaths = stimFramePaths,
                                                   ntrials = ntrials,
                                                   contrast = contrast,
                                                   dur = dur,
                                                   size = size,
                                                   reps = reps,
                                                   x=x,
                                                   y=y,
                                                   shuffle  = shuffle,
                                                   blankDur = blankDur,
                                                   decimationFactor = decimationFactor,
                                                   groupStimuliTrials = groupStimuliTrials,
                                                   mask = mask,
                                                   aperture = aperture,
                                                   printStimParams=printStimParams,
                                                   interpolate = interpolate,
                                                   progress = progress,
                                                   loopBack = loopBack,
                                                   background = background)
        
        self.rig = self.exp.rig
        print('Loaded stimuli for the {0} stimulus.'.format(self.stimName))
        self.last_position_data=list()
        self.max_velocity=0
        self.position_offset=0
        self.reward_enabled=0
        self.previous_position=0
        self.stim_pos=0
        self.rig.setRewardSize(rewardSize)
        print('[WheelTask] Reward size is {0}'.format(rewardSize))
        return

    def evolve(self):
        i = np.mod(self.expcnt,len(self.params))
        iFrame = self.iFrame-self.blankFrames
        iFrameStim = -1
        blank = 1
        code = 0
        
        x_offset=0
        screen_center=0
        max_displacement_degrees=60
        reward_zone_size_degrees=10
        give_reward_upon_stim_onset=1 # free reward on stim onset : phase 1
        give_reward_upon_zone_entry=1 # trigger 
               
        if self.iFrame==0:        
          self.free_reward_given=0

        pos_data=self.exp.rigstatus['position']
        #stim_on=self.exp.rigstatus['screen'] # not easy to get from the input, is event count
        #print(stim_on)
        self.delta_pos=pos_data[1]-self.previous_position
        self.previous_position=pos_data[1] # store for next loop
        if self.delta_pos>0 or self.delta_pos<0:
          #print(self.delta_pos)
          # only move towards center
          if np.sign(self.stim_pos) != np.sign(self.delta_pos) or np.sign(self.stim_pos) == 0:
            self.delta_pos = 0
          # the minus fixes the movement coupling
          self.stim_pos=self.stim_pos - self.delta_pos
          if self.stim_pos > max_displacement_degrees:
            self.stim_pos = max_displacement_degrees  
          elif self.stim_pos < -max_displacement_degrees:
            self.stim_pos = -max_displacement_degrees
          print(self.stim_pos)

        if self.iFrame >= self.blankFrames:
          # start of stim moving addon                  
          velocity=0
          if len(self.last_position_data)>0: # avoid causality error
            if (pos_data[0]-self.last_position_data[0])>0: # avoid invalid calculations
              velocity=(pos_data[1]-self.last_position_data[1]) / (pos_data[0]-self.last_position_data[0])
          self.last_position_data=pos_data

          
          if velocity>self.max_velocity:
            self.max_velocity=velocity
          #print(velocity)
          #print(self.max_velocity)
          # rescale velocity to [0 1]
          max_velocity=7000
          gain_factor=3 # assume animal cannot run at full speed
          velocity_scaled=np.absolute(velocity)/7000*gain_factor # measured max velocity possible
          if velocity_scaled>max_velocity:
            velocity_scaled=max_velocity
          #print(velocity_scaled)

          x_offset=self.stim_pos-self.params.iloc[i]['posx'][self.blankFrames]
        else: # since we always start with blank
          self.stim_pos=self.params.iloc[i]['posx'][self.blankFrames]
          if self.reward_enabled==1: # make sure reward is not given during blank
              self.exp.rig.disableRewardOnLap() # nothing to do with lap
              print('disabled reward')
              self.reward_enabled=0

        # only reward when stim is inside reward zone                
        if (self.stim_pos<reward_zone_size_degrees and self.stim_pos>-reward_zone_size_degrees):
          stim_in_reward_zone=1
        else:
          stim_in_reward_zone=0
        if self.iFrame >= self.blankFrames:
          if stim_in_reward_zone:
            if self.reward_enabled==0:
              if self.free_reward_given==0 and give_reward_upon_zone_entry:
                self.exp.rig.giveReward() # give reward upon reaching reward zone
                #self.free_reward_given=1 # uncomment to give only 1 reward for zone entry
                print('giving free reward on zone entry')
              self.exp.rig.enableRewardOnLap() # nothing to do with lap
              print('enabled reward')
              self.reward_enabled=1
          else:
            if self.reward_enabled==1:
              self.exp.rig.disableRewardOnLap() # nothing to do with lap
              print('disabled reward')
              self.reward_enabled=0
      
        if self.expcnt >= len(self.params) and not self.iFrame < self.blankFrames:
            self.exp.stop()
            return -1,[]
        elif self.iFrame < self.blankFrames:
            stim = self.blank
            iFrame = 0            
        elif self.iFrame >= self.blankFrames:            
            if self.free_reward_given==0 and stim_in_reward_zone==1 and give_reward_upon_stim_onset and self.iFrame==self.blankFrames:
              print(self.stim_pos)
              self.exp.rig.giveReward() # give reward upon reaching reward zone
              self.free_reward_given=1
              print('giving free reward at stim onset')

            blank = 0
            code = 1
            iStimFrame = self.params.iloc[i]['iStim']
            iFrameStim = np.mod(int(np.floor(iFrame/float(self.decimationFactor))),
                                len(self.stimuli[iStimFrame]))          
            stim = self.stimuli[iStimFrame][iFrameStim]
            stim.contrast = self.params.iloc[i]['contrast']
            stim.setSize(self.params.iloc[i]['size'])
          
            stim.pos = [self.params.iloc[i]['posx'][iFrame]+x_offset,
                        self.params.iloc[i]['posy'][iFrame]]
            if not self.aperture is None:
                self.aperture.setPos([self.params.iloc[i]['apx'][iFrame]+x_offset,
                                      self.params.iloc[i]['apy'][iFrame]])
                self.aperture.setSize(self.params.iloc[i]['apSize'])
                self.aperture.enable()
        if self.stimcnt != self.expcnt:
            # to update the indicator only at start
            self.stimcnt = self.expcnt
        toLog = [ self.params.iloc[i]['iStim']+1,
                  self.params.iloc[i]['iTrial']+1,
                  iFrameStim,
                  blank,
                  self.params.iloc[i]['contrast'],
                  self.params.iloc[i]['posx'][iFrame]+x_offset,
                  self.params.iloc[i]['posy'][iFrame]]
        stim.draw()
        if not self.aperture is None:
            self.aperture.disable()
            toLog += [self.params.iloc[i]['apx'][iFrame]+x_offset,
                      self.params.iloc[i]['apy'][iFrame]]
        self.iFrame += 1
        self.frameCnt += 1
        self.iStim = self.params.iloc[i]['iStim']
        if self.iFrame >= self.blankFrames:
            if iFrame == self.params.iloc[i].durationFrames-1:
                self.iFrame = 0
                self.expcnt += 1
        return code,toLog #[t for t in np.array(toLog)]