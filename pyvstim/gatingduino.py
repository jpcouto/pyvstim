#! /usr/bin/env python
# Class to interface with the arduino pmt gating controller.
#
import serial
import time
import sys
from multiprocessing import Process,Queue,Event,Value,Array
from ctypes import c_char_p,c_longdouble,c_long
import numpy as np
import re

import os
from os.path import join as pjoin
import csv
from datetime import datetime

import sys
if sys.version_info >= (3, 0):
    long = lambda x: int(x)

# ARDUINO MESSAGES
ERROR='E'
NOW = 'N'
STX='@'
SEP = '_'
ETX=serial.LF.decode()
ARM = 'A'
TRIGGER = 'T'
SET_PARAMETERS = 'P'

# Class to talk to arduino  using a separate process.
class PMTGateInterface(Process):
    def __init__(self,port='/dev/ttyACM0', baudrate=115200,
                 inQ = None, outQ=None,timeout=0.03):
        Process.__init__(self)
        if inQ is None:
            inQ = Queue()
        if outQ is None:
            outQ = Queue()
        self.inQ = inQ
        self.outQ = outQ
        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        try:
            self.ino = serial.Serial(port=self.port,
                                     baudrate=self.baudrate,
                                     timeout = self.timeout)
        except serial.serialutil.SerialException:
            raise(serial.serialutil.SerialException('''
Could not open arduino on port {0}

            Try logging out and back in.'''.format(self.port)))
        self.ino.flushInput()
        self.ino.close()
        self.display('Probed arduino on port {0}.'.format(port))

        self.corrupt_messages = 0
        self.exit = Event()        
        self.npulses = Value('i',10)
        self.width = Value('i',10)
        self.margin = Value('i',2)
        self.start()
    def close(self):
        self.exit.set()

    def display(self,msg):
        print('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)
        
    def ino_write(self, data):
        self.ino.write((STX + data + ETX).encode())
    
    def ino_read(self):
        msg = self.ino.readline().decode()
        return time.time(),msg

    def arm(self):
        self.inQ.put(ARM)

    def trigger(self):
        self.inQ.put(NOW)

    def set_parameters(self,npulses = None, width = None, margin = None):
        if npulses is None:
            npulses = self.npulses.value
        if width is None:
            width = self.width.value
        if margin is None:
            margin = self.margin.value
        self.inQ.put(SET_PARAMETERS+SEP+str(npulses) + SEP + str(width) + SEP + str(margin))
    def print_parameters(self):
        msg = '''
        PMT gate parameters:
             - npulses {0}
             - width {1}
             - margin {2}
        '''.format(self.npulses.value,
                   self.width.value,
                   self.margin.value)
        self.display(msg)
    def process_message(self, tread, msg):
        #treceived = long((tread - self.expStartTime.value)*1000)
        #print(msg)
        if msg.startswith(STX) and msg[-1].endswith(ETX):
            msg = msg.strip(STX).strip(ETX)
            if msg[0] == ARM:
                print('Arduino is armed.')
            elif msg[0] == TRIGGER:
                tmp = msg.split(SEP)
                print('Arduino is triggered.')
                
            elif msg[0] == SET_PARAMETERS:
                tmp = msg.split(SEP)
                self.npulses.value = int(tmp[1])
                self.width.value = int(tmp[2])
                self.margin.value = int(tmp[3])
                self.print_parameters()
            else:
                print('Unknown message: ' + msg)
        else:
            self.corrupt_messages += 1
            self.display('Error on msg [' + str(self.corrupt_messages) + ']: '+ msg.strip(STX).strip(ETX))
            return '# ERROR {0}'.format(msg)

    def run(self):
        self.ino = serial.Serial(port=self.port,
                                 baudrate=self.baudrate,
                                 timeout = self.timeout)
        self.ino.flushInput()
        time.sleep(0.5) # So that the first cued orders get processed
        self.set_parameters()
        while not self.exit.is_set():
            # Write whatever needed
            if not self.inQ.empty():
                message = self.inQ.get()
                self.ino_write(message)
            # Read whatever is there
            if (self.ino.inWaiting() > 0):
                tread,message = self.ino_read()
                try:
                    toout = self.process_message(tread,message)
                except:
                    self.display('ERROR with: ' + message)
                finally:
                    self.outQ.put(toout)
                    
        self.ino.close()
        self.display('Ended communication with arduino.')
