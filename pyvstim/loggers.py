import serial
import time
import sys
from multiprocessing import Process, Queue, Event, Value, Array
from ctypes import c_char_p,c_longdouble,c_long
import numpy as np
import re
from .utils import getPreferences,PYVSTIM_CODE_VERSION
from os.path import join as pjoin
import os
from datetime import datetime
import csv

ETX = '\n'

class Logger(Process):
    version = '1.4'
    def __init__(self,filename='run01_none.log',
                 inQ = None, log = True,preferences = None):
        
        Process.__init__(self)
        self.filename = Array('u',' '*1024)
        self.inQ = inQ
        self.logfile = None
        self.doLog = log
        self.exit = Event()
        if preferences is None:
            prefs = getPreferences()
        else:
            prefs = preferences
        self.logfolder = os.path.abspath(prefs['logFolder'])
        self.tmpfolder = os.path.abspath(prefs['tmpFolder'])
        self.isLogging = Event()
        self.setFileName(filename)
        self.daemon = True
        
    def display(self,msg):
        print('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)

    def startlog(self):
        if self.isLogging.is_set():
            self.stoplog()
        self.isLogging.set()

    def stoplog(self):
        self.isLogging.clear()
        if not self.logfile is None:
            while not self.inQ.empty():
                tmp = self.inQ.get()
                if not tmp is None:
                    self.csvwriter.writerow(tmp)
                else:
                    break
            self.logfile.close()
            self.display('Closed logging to : ' + os.path.basename(
                self.filepath))
            filename = self.getFilename()
            logfilename = os.path.abspath(pjoin(self.logfolder,filename))
            logfolder = os.path.dirname(logfilename)
            if not os.path.isdir(logfolder):
                self.display('Created log folder [{0}]'.format(logfolder))
                os.makedirs(logfolder)
            if not self.filepath == logfilename:
                if not os.path.exists(logfilename):
                    os.rename(self.filepath,logfilename)
                    self.display('Moved file to: ' + logfilename)
                else:
                    nfname = logfilename.replace('.log',
                                                 '{0}_cp-err.log'.format(
                                                     np.random.randint(1000000)))
                    os.rename(self.filepath,nfname)
                    


                    
                    print('''

    ----------------------------------
        IMPORTANT PYVSTIM WARNING
    ----------------------------------
     The file:

        {0}

    was already in the folder so it was saved as:

        {1}

    This is not normal. 


    You'll need to rename the files manually. 



                    Good luck...



                    '''.format(logfilename, nfname))
                    
        self.logfile = None

    def put(self,toLog):
        self.inQ.put(toLog)
    
    def newLog(self):
        filename = self.getFilename()
        fname = os.path.basename(filename)
        if not os.path.isdir(self.tmpfolder):
            os.makedirs(self.tmpfolder)
        filepath = pjoin(self.tmpfolder,fname)
        if not self.logfile is None:
            self.stoplog()
        self.isLogging.set()
        self.filepath = filepath
        self.logfile = open(filepath,'w')
        self.csvwriter = csv.writer(self.logfile, delimiter=',')
        self.logfile.write(
            '# Version ' +
            self.version + ETX)
        self.logfile.write(
            '# VStim git commit hash: {0}'.format(PYVSTIM_CODE_VERSION) +
            self.version + ETX)
        self.logfile.write(
            '# ' +
            datetime.today().strftime('%d-%m-%Y - %H:%M:%S') +
            ETX)
        self.logfile.write(
            '# Filename: {0}'.format(
                os.path.basename(self.getFilename())) + ETX)
        self.display('Started logging to : ' + os.path.basename(filepath))
        

    def setFileName(self,filename):
        self.stoplog()
        for i in range(len(self.filename)):
            self.filename[i] = ' '
        for i in range(len(filename)):
            self.filename[i] = filename[i]
        self.display('Filename [{0}] set'.format(self.getFilename()))
        
    def getFilename(self):
        return str(self.filename[:]).strip(' ')
        
    def run(self):
        while not self.exit.is_set():
            while self.isLogging.is_set():
                if self.logfile is None:
                    self.newLog()
                if not self.inQ.empty():
                    tmp = self.inQ.get()
                    if not tmp is None:
                        if not type(tmp) is list:
                            if tmp.startswith('#'):
                                self.logfile.write(
                                    tmp + ETX)
                        else:    
                            self.csvwriter.writerow(tmp)
            if not self.logfile is None:
                print('Closing log from the inside.')
                self.stoplog()
            time.sleep(0.005)
        print('Logger thread stopped.')
    def close(self):
        self.isLogging.clear()
        self.exit.set()
        print('Set logger exit flag.')
