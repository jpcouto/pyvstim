# pyVSTIM #

Code to deliver visual stimuli and log experiments.

pyVSTIM uses psychopy 

### Preference file ###

* pyVSTIM searches for preference files in the "vstim" folder of the user home directory.
* The preferences.json file has the global configurations for the user and points to the protocol files.

##### Preference files #####

Preference files are JSON files that are copied to the log directory everytime a protocol is ran from the GUI and are used for:

* monitor preferences ["monitor"]
* location of the experiment indicator for the photodiode ["flashIndicatorParameters"]
* controlling stimulus warp ["warp"]
* scanbox ["scanbox"] and labcams ["labcams"] ip locations/ default options of the checkboxes
* rig ["rig"] connection port
* user prefix that is appended to the and experiment names ["userPrefix"]
* protocols ["protocolsFolder"]; stimuli ["stimsFolder"]; tmp ["tmpFolder"]  and log ["logFolder"] paths location

Example files [here](https://bitbucket.org/activision/pyvstim/src/master/preferences/).

### How do I get set up? ###

* Summary of set up
* Configuration

 ```conda install numpy scipy matplotlib pandas pyopengl pillow lxml openpyxl xlrd configobj pyyaml gevent greenlet msgpack-python psutil pytables requests[security] cffi seaborn wxpython cython future pyzmq pyserial tqdm pip bokeh jupyter```

```conda install -c conda-forge pyglet python-bidi moviepy pyosf```

```pip install zmq json-tricks pyparallel sounddevice pygame pysoundcard psychopy_ext psychopy```

Download and install pyvstim and linearTreadmillRig go to bit bucket, clone and do ```python setup.py develop``` for each.


* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact