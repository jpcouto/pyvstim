import socket 
from os.path import join as pjoin
import time
from .gui import QWidget,QFormLayout,QLabel,QPushButton,QSlider,QCheckBox,Qt,qWait,QLineEdit
from .utils import display
import os
from os.path import join as pjoin

class ScanboxController(socket.socket):
    def __init__(self,ip=None, port = None, prefs = dict(startcommand = 'g',durationcommand = 'd',stopcommand = 'S')):
        super(ScanboxController,self).__init__(socket.AF_INET, socket.SOCK_DGRAM)
        # set up udp comm
        self.IP = ip       #"10.86.1.130"
        self.PORT = port   #7000 
        self.nFrames =   None
        self.prefs = prefs
        
    def sendMsg(self,msg):
        if not self.IP is None:
            self.sendto(msg.encode()+b'\n', (self.IP, self.PORT))
            print("Sending udp message to SBX: %s" % msg)
            qWait(200)
          
    def setExperiment(self,dataFolder,session,duration):
        self.sendMsg('E000') # reset experiment counter in SB GUI
        self.sendMsg('D%s' % dataFolder) # set dir name       
        self.sendMsg('A%s' % session) # set experiment name
        duration += 2.*5
        self.sendMsg(self.prefs['durationcommand'] + '%d' % int(duration)) # DURATION DO IT!
        display('Setting scanbox experiment [{0}] Duration = {1}'.format(self.IP,duration))
    def stopAcquisition(self):
        if not self.prefs['stopcommand'] == '':
            self.sendMsg(self.prefs['stopcommand'])
        #print('For some reason scanbox is not listening to the "S" command so this is disabled for now.')
    def startAcquisition(self):
        self.sendMsg(self.prefs['startcommand'])

class SpikeGLxController(socket.socket):
    def __init__(self,ip=None, port = None,
                 root_folder = None,
                 root_folder_map = None,
                 prefs = dict(startcommand = 'STARTRUN',
                              stopcommand = 'STOPRUN')):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        super(SpikeGLxController,self).__init__(socket.AF_INET, socket.SOCK_STREAM)
        # set up udp comm
        self.IP = ip       #"10.86.1.130"
        self.PORT = port   #7000 
        self.nFrames = None
        self.prefs = prefs
        self.waittime = 100
        self.root_folder = root_folder
        self.root_folder_map = root_folder_map
    def sendMsg(self,msg,BUFFER_SIZE = 2):
        print("Sending udp message to spikeglx: %s" % msg)
        self.connect((self.IP, self.PORT))
        self.send(msg+"\n")
        data = self.recv(BUFFER_SIZE)
        print(data)
        self.close()
        qWait(self.waittime)
          
    def setExperiment(self,dataFolder,session):
        if not self.root_folder_map is None:
            folder = pjoin(self.root_folder_map,dataFolder)
            if not os.path.isdir(folder):
                print('Creating folder {0}.'.format(pjoin(self.root_folder,dataFolder)))
                os.makedirs(folder)
        self.sendMsg('SETRUNDIR {0}'.format(pjoin(self.root_folder,dataFolder)))
        self.sendMsg('SETRUNNAME {0}'.format(session)) 
        display('Setting spikeGLX folder name [{0}]'.format(self.IP))
    def stopAcquisition(self):
        if not self.prefs['stopcommand'] == '':
            self.sendMsg(self.prefs['stopcommand'])
    def startAcquisition(self):
        self.sendMsg(self.prefs['startcommand'])


class LabcamsController(object):
    def __init__(self,ip=None, port = None):
        super(LabcamsController,self).__init__()
        # set up udp comm
        self.IP = ip       #"10.86.1.130"
        self.PORT = port   #7000
        import zmq
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REQ)
        self.socket.connect('tcp://{0}:{1}'.format(self.IP,self.PORT))
    def sendMsg(self,msg):
      self.socket.send_pyobj(msg,flags=zmq.NOBLOCK)
      msg = self.socket.recv_pyobj()
      print(msg)
    def setExperiment(self,dataFolder,session):
        self.sendMsg({'action':'expName','value':dataFolder + '\\' + session})
        display('Set labcams experiment')
    def setSave(self):
        self.sendMsg({'action':'trigger'})
        display('Triggered save on the cameras')

class PMTGatingController(QWidget):
    def __init__(self,ino=None):
        super(PMTGatingController,self).__init__()
        self.ino = ino
        self.tstart = time.time()
        form = QFormLayout()
        if not ino is None:
            def trigger():
                ino.trigger()
            triggerButton = QPushButton('Trigger')
            triggerButton.clicked.connect(trigger)
            form.addRow(triggerButton)

            def arm():
                ino.arm()
            armButton = QPushButton('Arm')
            armButton.clicked.connect(arm)
            form.addRow(armButton)
        
            self.wnpulses = QLineEdit(str(ino.npulses.value))
            self.wwidth = QLineEdit(str(ino.width.value))
            self.wmargin = QLineEdit(str(ino.margin.value))
            wparametersButton = QPushButton('Set parameters')
            wparametersButton.clicked.connect(self.setParameters)
            form.addRow(QLabel('Number of pulses'),self.wnpulses)
            form.addRow(QLabel('Width'),self.wwidth)
            form.addRow(QLabel('PMT margin'),self.wmargin)
            form.addRow(wparametersButton)
        self.setLayout(form)
        
    def setParameters(self):
        try:
            npulses = int(self.wnpulses.text())
            width = int(self.wwidth.text())
            margin = int(self.wmargin.text())
        except:
            print('Could not interpret parameters.')
        finally:
            self.ino.set_parameters(npulses,width,margin)

class ServoStageController(QWidget):
    def __init__(self,servostage=None):
        super(ServoStageController,self).__init__()
        self.servostage = servostage
        form = QFormLayout()
        if not servostage is None:
            def trigger():
                self.servostage.servo_act()
            actButton = QPushButton('Act')
            actButton.clicked.connect(trigger)
            form.addRow(actButton)
            def flip():
                self.servostage.servo_flip()
            flipButton = QPushButton('Flip servo')
            flipButton.clicked.connect(flip)
            form.addRow(flipButton)
            self.wposition = QLineEdit(str(0))
            form.addRow(QLabel('Position'),self.wposition)
            def go():
                self.servostage.move_to(float(self.wposition.text()))
            self.waccel = QLineEdit(str(self.servostage.acceleration))
            form.addRow(QLabel('Acceleration'),self.waccel)
            self.wvel = QLineEdit(str(self.servostage.velocity))
            form.addRow(QLabel('Velocity'),self.wvel)
            def setav():
                self.servostage.set_speed(float(self.waccel.text()),float(self.wvel.text()))
            self.wvel.returnPressed.connect(setav)
            self.waccel.returnPressed.connect(setav)
            self.wposition.returnPressed.connect(go)
            def arm():
                self.servostage.arm(position = float(self.wposition.text()))
            armButton = QPushButton('Arm')
            armButton.clicked.connect(arm)
            form.addRow(armButton)
        self.setLayout(form)        
