from multiprocessing import Process,Queue,Event,Value,Array
from mcculw import ul
from mcculw.enums import ULRange,ScanOptions,FunctionType,Status
from mcculw.ul import ULError
import ctypes
import numpy as np
import time
def memhandle_as_ctypes_array(memhandle):
    return ctypes.cast(memhandle, ctypes.POINTER(ctypes.c_ushort))

class MccdaqAnalogSample(Process):
    def __init__(self, board, channels = [0],ai_range = 1,srate = 10000):
        '''Reads analog channels from an mccdaq device like the USB 1208FS'''
        Process.__init__(self)
        self.board = board
        self.channels = channels
        self.num_channels = len(channels)
        self.buffersize = 3100
        self.srate = srate
        self.queue = Queue()
        self.range = ai_range
        self.event_running = Event()
        self.event_trigger = Event()
        self.daemon = True
        
    def run(self):
        # init board
        memhandle = ul.win_buf_alloc(self.buffersize)
        ctypes_array = memhandle_as_ctypes_array(memhandle)
        # initialize the board
        ul.a_in_scan(self.board,
                     np.min(self.channels),
                     np.max(self.channels),
                     self.buffersize,
                     self.srate,
                     self.range,
                     memhandle,
                     ScanOptions.BACKGROUND | ScanOptions.CONTINUOUS)
        status, curr_count, curr_index = ul.get_status(self.board, FunctionType.AIFUNCTION)
        self.event_running.set()
        buff = np.zeros(self.buffersize)
        last_count = 0
        # Use the start count to be able to trigger from software
        while self.event_running.is_set():
            status, curr_count, curr_index = ul.get_status(self.board, 
                                                           FunctionType.AIFUNCTION)
            start_count = last_count+1
            start_index = np.mod(last_count,self.buffersize)
            end_index = np.mod(curr_count,self.buffersize)
            if end_index < start_index:
                print('Loop on buffer. ' + str(np.divide(curr_count,self.buffersize)))
                end_index = self.buffersize
                curr_count = np.divide(curr_count,self.buffersize)*self.buffersize
            last_count = curr_count
            for iSample in range(start_index, end_index):
                eng_value = ul.to_eng_units(self.board,
                                            self.range,
                                            ctypes_array[iSample])
                buff[iSample] = eng_value
            if self.event_trigger.is_set():
                # send to queue
                print('Queue: '+ str(end_index - start_index) + ' - ' + str(last_count))
                self.queue.put(buff[start_index:end_index].copy())
            time.sleep(0.03)
        ul.stop_background(self.board, FunctionType.AIFUNCTION)
