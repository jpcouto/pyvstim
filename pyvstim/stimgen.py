import numpy as np

def freqspace(n = 128):
    ''' 
    Mimic matlab freqspace with meshgrid option and for 2 dims.
    returns X,Y in the same order as matlab.
    '''
    f1 = (np.arange(0,n,dtype=float)-np.floor(n/2.)) * (2/float(n))
    return np.meshgrid(f1,f1)
    
def bandpass_noise(npix=128,width=60,
                   stimlen = 60*4,
                   bandwidth = 0.5,
                   sf_c=0.5,
                   sf_0=0.02,
                   tf_0=0.02,
                   refresh_rate = 60.,
                   beta=-1,
                   contrast=1,
                   sigmarange=3):        
    # Spatial frequency grig
    Fxy = float(npix/width); # samples / deg
    ux,uy = freqspace(npix)
    vxy = Fxy/2.*np.sqrt(ux**2.+uy**2.)

    # Temporal frequency grid
    ut = ((np.arange(0,stimlen)-np.floor(stimlen/2.)) * float(2./stimlen)).astype(float)
    vt = (refresh_rate/2.)*ut;
    #import pylab as plt
    #plt.figure()
    #plt.plot(vt)
    #plt.show()
    # Amplitude spectrum, space
    Hxy = ((vxy+sf_c)**beta)*np.abs(vxy<=sf_0)
    Hxy = np.transpose(Hxy[...,None] + np.zeros(stimlen)[None,None,:],[2,0,1])
    #print(Hxy.shape)
    #print(vt.shape)
    # Amplitude spectrum, time
    if tf_0 > 0:
        Ht = np.abs(vt) <= tf_0
        Ht = np.transpose(Ht[None,None,:]+np.zeros_like(Hxy[0])[...,None],[2,0,1])
        
    #print(Ht.shape)
    # space-time
    H =Hxy*Ht;
    x = np.random.randn(stimlen,npix,npix)

    X = np.fft.fftshift(np.fft.fftn(x));     
    Y = X*H;
    y = np.real(np.fft.ifftn(np.fft.fftshift(Y)));

    sigma = np.std(y);
    lims = sigmarange*np.array([-1.*sigma,sigma]); 
    return np.clip((((y*contrast)-lims[0])/(lims[1]-lims[0])*255),0,255).astype(np.uint8)


def circle_pix(size = 64, center = (0,0), radius = 20, 
                        foreground = 255, background = 0):
    x = np.linspace(-size/2, size/2, size)
    y = np.linspace(-size/2, size/2, size)
    X, Y = np.meshgrid(x, y)
    circle = background*np.ones((size, size), dtype = np.uint8)
    mask = (X - center[0])**2 + (Y - center[1])**2 <= radius**2
    circle[mask] = foreground
    return np.uint8(np.clip(circle,0,255))


def grid_squares(value = 0, gridsize = [10,10], background = 128):
    ''' 
    Creates a grid of squares that can be used for instance for receptive field mapping.
    Usage:
    
        stimarray = grid_squares(value=0,gridsize=[10,5], background  = 128)
        
    '''
    nsquares = np.multiply(*gridsize)
    stimarray = np.zeros((nsquares,gridsize[0],gridsize[1]),dtype = np.uint8)
    stimarray[:] = background

    for iF in range(nsquares):
        iX = int(iF/gridsize[1])
        iY = np.mod(iF,gridsize[1])
        stimarray[iF,iX,iY] = value
    return stimarray

# Plotting and so on..

def nb_display_stim_array(stimarray,interval = 16):
    import pylab as plt
    from matplotlib.animation import FuncAnimation
    
    fig, ax = plt.subplots()
    ln = plt.imshow(stimarray[0],clim=[0,255],cmap='gray')
    txt = plt.text(0,0,'frame {0}'.format(0),color = 'r')
    plt.axis('off')
    def update(frame):
        ln.set_data(stimarray[frame])
        txt.set_text('frame {0}'.format(frame))
        return ln,

    ani = FuncAnimation(fig, update,
                        frames=range(len(stimarray)),
                        blit=True, repeat = True,
                        interval = interval,
                        repeat_delay = 100)
    return ani  # to stop click the plot and press q

def save_picstims_and_protfile(stim,stims_folder,protocol_name,
                               stimpar = dict(black_duration = 0.1,
                                              duration = 1,
                                              width = 60,
                                              contrast = 1,
                                              decimation_ratio = 1,
                                              background = 0,
                                              blank_duration = 1,
                                              ntrials = 10,
                                              shuffle = True,
                                              interpolate = True),
                               file_rule = 'stim{0:03d}_frame{1:03d}.png',
                               protocols_folder=None):
    ''' Saves a stimulus array to png pictures and prepares image display protocol file.
    Usage:
    stimpar = dict(black_duration = 0.1,
               duration = 0.1,
               width = width,
               contrast = 1,
               decimation_ratio = 1,
               background = 0,
               blank_duration = 0.1,
               ntrials = 20,
               shuffle = True,
               interpolate = False)

    prefs = getPreferences(user = user_name)
    assert 'stimsFolder' in prefs.keys(), 'Can not continue: Add "stimsFolder" to the preference file [user: {0}]'.format(
        user_name)
    stims_folder = prefs['stimsFolder']
    protocols_folder = prefs['protocolsFolder']
    stim = [s for s in stimarray]
        
    save_stim_array_to_pictures(stim,
                                stims_folder,
                                protocol_name,
                                stimpar,
                                protocols_folder = protocols_folder)
    '''
    import os
    from os.path import join as pjoin
    from imageio import imwrite    
    # stimuli must be a list
    assert type(stim) is list, 'Must pass a list of stimuli!' 
    nstims = len(stim)
    folder = os.path.abspath(pjoin(stims_folder,protocol_name))
    if not os.path.isdir(folder):
        print('Created folder {0}'.format(folder))
        os.makedirs(folder)
    else:
        print('Warning! Folder {0} was already there. This will overwrite but the number of stimuli might have changed.'
              .format(folder))

    for iStim,stimulus in enumerate(stim):
        if len(stimulus.shape) == 2:
            stimulus = [stimulus]
        for iFrame,frame in enumerate(stimulus):
            imwrite(pjoin(folder,file_rule.format(iStim,iFrame)), frame.astype('uint8'))

    stimpar = dict(stimpar,
                   file_format = file_rule,
                   protocol_name = protocol_name)

    protfile = '''StimulusType = {protocol_name}
PicsFolder = {protocol_name}
PicsNameFormat = {file_format}
BlankDuration = {blank_duration}
DecimationRatio = {decimation_ratio}
WindowType = rect
Background = {background}
nTrials = {ntrials}
Shuffle = {shuffle}
Interpolate = {interpolate}
n	dur len	c	width	xc	yc
'''.format(**stimpar)
    stimstr = '{iStim}\t{duration}\t{nframes}\t{contrast}\t{width}\t0\t0 \n'

    for iStim in range(nstims):
        nframes = len(stimulus)
        protfile += stimstr.format(iStim = iStim,nframes = nframes,**stimpar)
    # write to folder
    with open(pjoin(folder,protocol_name+'.prot'),'w') as f:
        f.write(protfile)
    # write to protocols
    if not protocols_folder is None:
        fname = pjoin(protocols_folder,protocol_name+'.prot')
        if os.path.isfile(fname):
            print('Warning! File exists {0}, did not overwrite.'.format(fname))
        else:
            with open(fname,'w') as f:
                f.write(protfile)
